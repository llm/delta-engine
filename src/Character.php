<?php

require_once __DIR__.'/../const.php';
require_once __DIR__.'/../lib/Map.php';
use Doctrine\Common\Collections\ArrayCollection;
use Game\Map;
/**
 * @Entity @Table(name="characters")
 **/
class Character
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $name;
    /** @Column(type="string", nullable=true) **/
    protected $avatar;
    /** @Column(type="integer") **/
    protected $gender=50;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $xp=0;
    /** @Column(type="integer",options={"default"=1}) **/
    protected $lvl=1;
    /** @Column(type="integer",options={"default"=100}) **/
    protected $hp=100;
    /** @ManyToOne(targetEntity="CharacterClass")
     * @var CharacterClass
     */
    private $cClass;
    /** @ManyToOne(targetEntity="Player", inversedBy="characters")
     * @var Player
     */
    private $player;
    /**
     * @ManyToOne(targetEntity="GameSession", inversedBy="characters")
     * @var GameSession
     **/
    private $gamesession;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $nbdeath=0;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $x=0;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $y=0;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $z=0;
    /** @Column(type="integer",options={"default"=6}) **/
    protected $ap=6;
    /** @Column(type="boolean") **/
    protected $turnended=false;
    /** @Column(type="integer") **/
    protected $range;
    /** @Column(type="boolean",options={"default"=false}) **/
    protected $bot=false;
    /** @Column(type="string", nullable=true) **/
    protected $token;
    
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_head;
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_suit;
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_boots;
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_mainarm;
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_weakarm;
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_mainhand;
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_weakhand;
    /** @ManyToOne(targetEntity="Equipment") 
     * @var Equipment
    **/
    private $equipment_chest;
    
    public function __construct($player,$name,$class)
    {
		$this->player = $player;
        $this->name = $name;
        $this->cClass = $class;
        $this->range = $class->getRange();
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
    
    public function getAvatar()
    {
        return $this->avatar;
    }
    
    public function setGender($gender)
    {
        $this->gender = $gender;
    }
    
    public function getGender()
    {
        return $this->gender;
    }
    
    public function getPlayer()
    {
		return $this->player;
	}
    
    public function getLimitedProfile()
    {
		$profile = array(
			'name'=>$this->getName(),
			'avatar'=>$this->getAvatar(),
			'token'=>$this->getToken(),
			'x'=>$this->getX(),
			'y'=>$this->getY(),
			'z'=>$this->getZ(),
			'bot'=>$this->isBot(),
			'turnended'=>$this->hasEndedTurn()
		);
		if (!is_null($this->getGameSession()))
		{
			$profile['gamesessionid'] = $this->getGameSession()->getId();
		}
		return $profile;
	}
	
	public function getCharacterClass()
	{
		return $this->cClass;
	}
	
	public function getXp()
	{
		return $this->xp;
	}
	
	public function setXp($xp)
	{
		$this->xp = $xp;
	}
	
	public function getHp()
	{
		return $this->hp;
	}
	
	public function setHP($hp)
	{
		if ($hp <= 0)
		{
			$this->kill();
			$hp = 0;
		}
		$this->hp=$hp;
	}
	
	public function kill()
	{
		$player = $this->getPlayer();
		$faction = $player->getFaction();
		$newxp = round($this->getXp() * $faction->getXPSave());
		$this->setXp($newxp);
		$this->nbdeath = $this->nbdeath + 1;
	}
    
    public function getLevel()
    {
		return $this->lvl;
	}
	
	public function toggleBot()
	{
		$this->bot=!$this->bot;
	}
	
	public function isBot()
	{
		return $this->bot;
	}
	
	public function setLevel($level)
	{
		$this->lvl = $level;
	}
	
	public function getGameSession()
	{
		return $this->gamesession;
	}
	
	public function setGamesession($gamesession)
	{
		$this->gamesession = $gamesession;
	}
    
    public function getProfile()
    {
		$profile = array(
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'avatar'=>$this->getAvatar(),
			'token'=>$this->getToken(),
			'gender'=>$this->getGender(),
			'classid'=>$this->getCharacterClass()->getId(),
			'xp'=>$this->getXp(),
			'ap'=>$this->getAP(),
			'hp'=>$this->getHp(),
			'level'=>$this->getLevel(),
			'x'=>$this->getX(),
			'y'=>$this->getY(),
			'z'=>$this->getZ(),
			'bot'=>$this->isBot(),
			'turnended'=>$this->hasEndedTurn(),
			'repair'=>$this->getRepair(),
			'pilot'=>$this->getPilot(),
			'medic'=>$this->getMedic(),
			'engineering'=>$this->getEngineering(),
			'speed'=>$this->getSpeed(),
			'range'=>$this->getRange(),
			'equipments'=>$this->getEquipmentsDescription(),
			'nbdeath'=>$this->nbdeath
		);
		if (!is_null($this->getGameSession()))
		{
			$profile['gamesessionid'] = $this->getGameSession()->getId();
		}
		return $profile;
	}
	
	public function getMaxTurn()
	{
		return MIN_TURN + round($this->getLevel()/$this->getCharacterClass()->getTurnDivider(),0,PHP_ROUND_HALF_DOWN);
	}
	
	public function describe()
	{
		return $this->getLimitedProfile();
	}
	
	public function getRepair()
	{
		return $this->getCharacterClass()->getRepair($this->getLevel());
	}
	
	public function getPilot()
	{
		return $this->getCharacterClass()->getPilot($this->getLevel());
	}
	
	public function getMedic()
	{
		return $this->getCharacterClass()->getMedic($this->getLevel());
	}
	
	public function getEngineering()
	{
		return $this->getCharacterClass()->getEngineering($this->getLevel());
	}
	
	public function getSpeed()
	{
		return $this->getCharacterClass()->getSpeed();
	}
	
	public function getRange()
	{
		return $this->getCharacterClass()->getRange();
	}
	
	public function getX()
	{
		return $this->x;
	}
	
	public function getY()
	{
		return $this->y;
	}
	
	public function getZ()
	{
		return $this->z;
	}
	
	public function setX($x)
	{
		$this->x = $x;
	}
	
	public function setY($y)
	{
		$this->y = $y;
	}
	
	public function setZ($z)
	{
		$this->z = $z;
	}
	
	public function extinguishFire()
	{
		if (!$this->hasEndedTurn())
		{
			if ($this->getAP() > 0)
			{
				$mission = $this->getGameSession()->getMission();
				$map = new Map($mission->getMap());
				$maparr = json_decode($mission->getMap(),true);
				$act=false;
				if ($map->hasFireInRange($this->getX(),$this->getY(),$this->getZ(),$this->getRange()))
				{
					foreach ($maparr['fire_zone'] as $key => &$node)
					{
						if (abs($node['x']-$this->getX()) <= $this->getRange() && abs($node['y']-$this->getY()) <= $this->getRange())
						{
							if ($maparr['dimensions'] == 2 || abs($node['z']-$this->getZ()) <= $this->range())
							{
								$node['value'] = $node['value'] - 1;
								if ($node['value'] == 0)
								{
									unset($maparr['fire_zone'][$key]);
								}
							}
						}
					}
					$act = true;
					$mission->setMap($maparr);
				}
				
				if ($act)
				{
					$this->setAP($this->getAP()-1);
				}
				return $act;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public function repair()
	{
		if (!$this->hasEndedTurn())
		{
			if ($this->getAP() > 0)
			{
				$mission = $this->getGameSession()->getMission();
				$map = new Map($mission->getMap());
				$act=false;
				if ($map->hasFTLInRange($this->getX(),$this->getY(),$this->getZ(),$this->getRange()))
				{
					if ($mission->getFTLHP() > 0 || $this->getGameSession()->canRepairOnZero())
					{
						$mission->repairFTL($this->getRepair());
						$act = true;
					}
				}
				if ($map->hasSSInRange($this->getX(),$this->getY(),$this->getZ(),$this->getRange()))
				{
					if ($mission->getSurviveSystemsHP() > 0 || $this->getGameSession()->canRepairOnZero())
					{
						$mission->repairSurviveSystems($this->getRepair());
						$act = true;
					}
				}
				if ($map->hasCommunicationInRange($this->getX(),$this->getY(),$this->getZ(),$this->getRange()))
				{
					if ($mission->getCommunicationHP() > 0 || $this->getGameSession()->canRepairOnZero())
					{
						$mission->repairCommunications($this->getRepair());
						$act = true;
					}
				}
				if ($act)
				{
					$this->setAP($this->getAP()-1);
				}
				return $act;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/*
	 * This method can only move the character at maximum of 1 square of its current position
	 */ 
	public function moveToCoordonates($x,$y,$z=0)
	{
		if (!$this->hasEndedTurn())
		{
			if ($this->getAP() > 0)
			{
				if ($x > 0 && $y > 0 && $z >= 0 && $x <= $this->getGameSession()->getMission()->getType()->getMaxX() && $y <= $this->getGameSession()->getMission()->getType()->getMaxY() && $z <= $this->getGameSession()->getMission()->getType()->getMaxZ())
				{
					if (abs($this->getX()-$x) <= 1 && abs($this->getY()-$y) <= 1 && abs($this->getZ()-$z) <=1)
					{
						$map = new Map($this->getGameSession()->getMission()->getMap());
						if (!$map->isUnwalkable($x,$y,$z))
						{
							$this->setX($x);
							$this->setY($y);
							$this->setZ($z);
							$this->setAP($this->getAP()-1);
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/*
	 * This method only accepts +1/0/-1 values
	 */ 
	public function move($dx,$dy,$dz=0)
	{
		if (!$this->hasEndedTurn())
		{
			 if (abs($dx) <= 1 && abs($dy) <= 1 && abs($dz) <= 1)
			 {
				 $newx = $this->getX()+$dx;
				 $newy = $this->getY()+$dy;
				 $newz = $this->getZ()+$dz;
				 return $this->moveToCoordonates($newx,$newy,$newz);
			 }
			 else
			 {
				 return false;
			 }
		}
		else
		{
			return false;
		}
	}
	
	public function getAP()
	{
		return $this->ap;
	}
	
	public function setAP($ap)
	{
		$this->ap = $ap;
	}
	
	public function addXP($xp)
	{
		$this->setXp($this->getXp()+$xp);
	}
	
	public function removeXP($xp)
	{
		$newxp = $this->getXp()-$xp;
		if ($newxp < 0)
		{
			$newxp = 0;
		}
		$this->setXp($newxp);
	}
	
	public function removeGameSession()
	{
		$this->setGamesession(null);
	}
	
	public function endTurn()
	{
		$this->turnended=true;
		$this->getGameSession()->endTurn();
	}
	
	public function newTurn()
	{
		$this->turnended=false;
	}
	
	public function hasEndedTurn()
	{
		return $this->turnended;
	}
	
	public function getReward()
	{
		$gamesession = $this->getGameSession();
		if (!is_null($gamesession) && $gamesession->isEnded())
		{
			if ($this->getLevel() < $this->getCharacterClass()->getMaxLevel())
			{
				$neededXp = $this->getCharacterClass()->getNextLevelXP($this->getLevel());
				$this->addXp($gamesession->getDifficulty()*XP_PER_DIFFICULTY_LEVEL);
				if ($this->getXp() >= $neededXp)
				{
					$this->setLevel($this->getLevel()+1);
				}
			}
			else
			{
				$this->addXp($gamesession->getDifficulty()*XP_PER_DIFFICULTY_LEVEL);
			}
			$missiontype = $gamesession->getMission()->getType();
			$player = $this->getPlayer();
			$faction = $player->getFaction();
			$gold = $faction->getGoldAfterTax(round($missiontype->getGoldReward()*$gamesession->getDifficulty()*GOLD_REWARD_PER_DIFFICULTY_LEVEL));
			$player->addGold($gold);
			$gamesession->removeCharacter($this);
		}
	}
	
	public function setToken($token)
	{
		$this->token=$token;
	}
	
	public function getToken()
	{
		return $this->token;
	}
	
	public function getMaxHP()
	{
		return CHARACTER_MAX_HP;
	}
	
	public function getEquipment($type)
	{
		$equipment;
		switch($type)
		{
			case 'head':
				$equipment = $this->equipment_head;
			break;
			case 'suit':
				$equipment = $this->equipment_suit;
			break;
			case 'boots':
				$equipment = $this->equipment_boots;
			break;
			case 'mainarm':
				$equipment = $this->equipment_mainarm;
			break;
			case 'weakarm':
				$equipment = $this->equipment_weakarm;
			break;
			case 'mainhand':
				$equipment = $this->equipment_mainhand;
			break;
			case 'weakhand':
				$equipment = $this->equipment_weakhand;
			break;
			case 'chest':
				$equipment = $this->equipment_chest;
			break;
		}
		return $equipment;
	}
	
	public function getEquipments()
	{
		$types = array('head','suit','boots','mainarm','weakarm','mainhand','weakhand','chest');
		$result = array();
		foreach ($types as $type)
		{
			$result[$type] = $this->getEquipment($type);
		}
		return $result;
	}
	
	public function getEquipmentsDescription()
	{
		$equipments = $this->getEquipments();
		$result = array();
		foreach ($equipments as $key=>$equipment)
		{
			if (!is_null($equipment))
			{
				$result[$key] = $equipment->describe();
			}
		}
		return $result;
	}
	
	public function equipWith($equipment)
	{
		$price = $equipment->getPrice();
		$player = $this->getPlayer();
		if ($player->getGold() < $price)
		{
			return false;
		}
		$type = $equipment->getType();
		$equiped = true;
		switch($type)
		{
			case 'head':
				$this->equipment_head = $equipment;
			break;
			case 'suit':
				$this->equipment_suit = $equipment;
			break;
			case 'boots':
				$this->equipment_boots = $equipment;
			break;
			case 'mainarm':
				$this->equipment_mainarm = $equipment;
			break;
			case 'weakarm':
				$this->equipment_weakarm = $equipment;
			break;
			case 'mainhand':
				$this->equipment_mainhand = $equipment;
			break;
			case 'weakhand':
				$this->equipment_weakhand = $equipment;
			break;
			case 'chest':
				$this->equipment_chest = $equipment;
			break;
			default:
				$equiped = false;
			break;
		}
		if ($equiped)
		{
			$player->removeGold($price);
		}
		return true;
	}
}
