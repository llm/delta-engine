<?php

namespace Tools;

require_once __DIR__.'/BasicEnum.php';

class Error extends BasicEnum {
	const INVALID_TOKEN = 1;
	const BAD_CREDENTIALS = 2;
}
