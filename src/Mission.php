<?php


/**
 * @Entity @Table(name="missions")
 **/
class Mission
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @ManyToOne(targetEntity="MissionType") **/
    private $type;
    /** @Column(type="integer") **/
    protected $hp;
    /** @Column(type="boolean") **/
    protected $FTLdrive=true;
    /** @Column(type="boolean") **/
    protected $surviveSystems=true;
    /** @Column(type="boolean") **/
    protected $communication=true;
    /** @Column(type="integer") **/
    protected $oxygenLevel=100;
    /** @Column(type="text") **/
    protected $map;
    /** @Column(type="integer") **/
    protected $ftlhp;
    /** @Column(type="integer") **/
    protected $communicationhp;
    /** @Column(type="integer") **/
    protected $survivesystemshp;
    
    public function __construct($type)
    {
		$this->type=$type;
		$this->map = $type->getMap();
		$this->hp = $type->getMaxHp();
		$this->ftlhp = $type->getMaxFTLHp();
		$this->survivesystemshp = $type->getMaxSurviveSystemsHp();
		$this->communicationhp = $type->getMaxCommunicationHp();
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setHP($hp)
	{
		$this->hp=$hp;
	}
	
	public function getHP()
	{
		return $this->hp;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function getFTLDrive()
	{
		return $this->FTLdrive;
	}
	
	public function getSurviveSystems()
	{
		return $this->surviveSystems;
	}
	
	public function getCommunications()
	{
		return $this->communication;
	}
	
	public function breakFTL()
	{
		$this->FTLdrive=false;
	}
	
	public function breakCommunications()
	{
		$this->communication=false;
	}
	
	public function breakSurviveSystems()
	{
		$this->surviveSystems=false;
	}
	
	public function repairFTL($hp)
	{
		$newhp = $this->getFTLHP() + $hp;
		if ($newhp >= $this->getType()->getMaxFTLHp())
		{
			$this->ftlhp = $this->getType()->getMaxFTLHp();
			$this->FTLdrive=true;
		}
		else
		{
			$this->ftlhp = $newhp;
		}
	}
	
	public function repairSurviveSystems($hp)
	{
		$newhp = $this->getSurviveSystemsHP()+$hp;
		if ($newhp >= $this->getType()->getMaxSurviveSystemsHp())
		{
			$this->survivesystemshp = $this->getType()->getMaxSurviveSystemsHp();
			$this->surviveSystems=true;
		}
		else
		{
			$this->survivesystemshp=$newhp;
		}
	}
	
	public function repairCommunications($hp)
	{
		$newhp = $this->getCommunicationHP()+$hp;
		if ($newhp >= $this->getType()->getMaxCommunicationHp())
		{
			$this->communicationhp = $this->getType()->getMaxCommunicationHp();
			$this->communication = true;
		}
		else
		{
			$this->communicationhp=$newhp;
		}
	}
	
	public function getMap()
	{
		return $this->map;
	}
	
	public function setMap($map)
	{
		if (is_array($map))
		{
			$map = json_encode($map);
		}
		$this->map = $map;
	}
	
	public function modifyMap($map)
	{
		$this->map = $map;
	}
	
	public function takeDamage($hp)
	{
		$this->setHP($this->getHP()-$hp);
	}
	
	public function oxygen($difficultylevel,$nb)
	{
		if (!$this->getSurviveSystems())
		{
			$consumption = round($difficultylevel*OXYGEN_CONSUMPTION_PER_DIFFICULTY_LEVEL*$nb);
			$this->consumeOxygen($consumption);
		}
		else
		{
			$this->produceOxygen();
		}
	}
	
	public function produceOxygen()
	{
		if ($this->oxygenLevel < 100)
		{
			$this->oxygenLevel = $this->oxygenLevel + OXYGEN_NEW_PER_TURN;
		}
		if ($this->oxygenLevel > 100)
		{
			$this->oxygenLevel = 100;
		}
	}
	
	public function consumeOxygen($consumption)
	{
		$this->oxygenLevel = $this->getOxygenLevel() - $consumption;
		if ($this->oxygenLevel < 0)
		{
			$this->oxygenLevel = 0;
		}
	}
	
	public function getOxygenLevel()
	{
		return $this->oxygenLevel;
	}
	
	public function getMaxHP()
	{
		return $this->getType()->getMaxHp();
	}
	
	public function getFTLHP()
	{
		return $this->ftlhp;
	}
	
	public function setFTLHP($hp)
	{
		$this->ftlhp = $hp;
	}
	
	public function getSurviveSystemsHP()
	{
		return $this->survivesystemshp;
	}
	
	public function setSurviveSystemsHP($hp)
	{
		$this->survivesystemshp = $hp;
	}
	
	public function getCommunicationHP()
	{
		return $this->communicationhp;
	}
	
	public function setCommunicationHP($hp)
	{
		$this->communicationhp=$hp;
	}
	
	public function cycle($difficultylevel,$nbpassengers)
	{
		if (!$this->getFTLDrive())
		{
			$damage = rand(1,MAX_DAMAGE_PER_DIFFICULTY_LEVEL*$difficultylevel);
			$newhp = $this->getFTLHP() - $damage;
			if ($newhp < 0)
			{
				$newhp = 0;
			}
			$this->setFTLHP($newhp);
		}
		if (!$this->getSurviveSystems())
		{
			$damage = rand(1,MAX_DAMAGE_PER_DIFFICULTY_LEVEL*$difficultylevel);
			$newhp = $this->getSurviveSystemsHP() - $damage;
			if ($newhp < 0)
			{
				$newhp = 0;
			}
			$this->setSurviveSystemsHP($newhp);
		}
		if (!$this->getCommunications())
		{
			$damage = rand(1,MAX_DAMAGE_PER_DIFFICULTY_LEVEL*$difficultylevel);
			$newhp = $this->getCommunicationHP() - $damage;
			if ($newhp < 0)
			{
				$newhp = 0;
			}
			$this->setCommunicationHP($newhp);
		}
		$this->oxygen($difficultylevel,$nbpassengers);
	}	
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'ftl'=>$this->getFTLDrive(),
			'survivesystems'=>$this->getSurviveSystems(),
			'communications'=>$this->getCommunications(),
			'oxygenlevel'=>$this->getOxygenLevel(),
			'hp'=>$this->getHP(),
			'ftlhp'=>$this->getFTLHP(),
			'maxftlhp'=>$this->getType()->getMaxFTLHp(),
			'survivesystemshp'=>$this->getSurviveSystemsHP(),
			'maxsurvivesystemshp'=>$this->getType()->getMaxSurviveSystemsHp(),
			'communicationhp'=>$this->getCommunicationHP(),
			'maxcommunicationhp'=>$this->getType()->getMaxCommunicationHp()
			
		);
		return $description;
	}
}
