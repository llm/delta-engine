<?php

// the only mandatory is th survive systems

/**
 * @Entity @Table(name="missiontypes")
 **/
class MissionType
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text", unique=true) **/
    protected $name;
    /** @Column(type="integer") **/
    protected $maxHp=1;
    /** @Column(type="integer") **/
    protected $price=10000;
    /** @Column(type="boolean") **/
    protected $buildable=true;
    /** @Column(type="integer") **/
    protected $maxplayers=2;
    /** @Column(type="json_array") **/
    protected $map;
    /** @Column(type="integer") **/
    protected $reward;
    /** @Column(type="integer") **/
    protected $maxftlhp;
    /** @Column(type="integer") **/
    protected $maxcommunicationhp;
    /** @Column(type="integer") **/
    protected $maxsurvivesystemshp;
    /** @Column(type="integer",options={"default"=10}) **/
    protected $maxx=10;
    /** @Column(type="integer",options={"default"=10}) **/
    protected $maxy=10;
    /** @Column(type="integer",options={"default"=1}) **/
    protected $maxz=1;

    public function getId()
    {
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}
	
	public function getTypeName()
	{
		return $this->getJSONMap()['type'];
	}

	public function getBuildable()
	{
		return $this->buildable;
	}

	public function isBuildable()
	{
		return $this->getBuildable();
	}
	
	public function setMaxHp($hp)
	{
		$this->maxHp=$hp;
	}
	
	public function getMaxHp()
	{
		return $this->maxHp;
	}

	public function setBuildable($buildable)
	{
		$this->buildable = $buildable;
	}
	
	public function setGoldReward($gold)
	{
		$this->reward = $gold;
	}
	
	public function getGoldReward()
	{
		return $this->reward;
	}

	public function setPrice($price)
	{
		$this->price=$price;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getFormatedPrice($decPoint='.',$thousandsSep=' ')
	{
		return Tools::getFormatedNumber($this->getPrice());
	}
	
	public function setMaxPlayers($max)
	{
		$this->maxplayers = $max;
	}
	
	public function getMaxPlayers()
	{
		return $this->maxplayers;
	}
	
	public function setMap($map)
	{
		$this->map = $map;
		$json = json_decode($map,true);
		$this->maxx = $json['size_x'];
		$this->maxy = $json['size_y'];
		if ($json['dimensions']==3)
		{
			$this->maxz = $json['size_z'];
		}
	}
	
	private function getJSONMap()
	{
		return json_decode($this->map,true);
	}
	
	public function hasFTLDrive()
	{
		return array_key_exists('ftl_drive',$this->getJSONMap());
	}
	
	public function hasCommunication()
	{
		return array_key_exists('communication',$this->getJSONMap());
	}
	
	public function hasPilot()
	{
		return array_key_exists('pilot',$this->getJSONMap());
	}
	
	public function getMaxX()
	{
		return $this->maxx;
	}
	
	public function getMaxY()
	{
		return $this->maxy;
	}
	
	public function getMaxZ()
	{
		return $this->maxz;
	}
	
	public function getMap()
	{
		return $this->map;
	}
	
	public function getMaxFTLHp()
	{
		return $this->maxftlhp;
	}
	
	public function getMaxCommunicationHp()
	{
		return $this->maxcommunicationhp;
	}
	
	public function getMaxSurviveSystemsHp()
	{
		return $this->maxsurvivesystemshp;
	}
	
	public function setMaxFTLHp($max)
	{
		$this->maxftlhp = $max;
	}
	
	public function setMaxCommunicationHp($max)
	{
		$this->maxcommunicationhp = $max;
	}
	
	public function setMaxSurviveSystemsHp($max)
	{
		$this->maxsurvivesystemshp = $max;
	}
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'maxplayers'=>$this->getMaxPlayers(),
			'buildable'=>$this->getBuildable()
		);
		return $description;
	}
}
