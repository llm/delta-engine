<?php

if(! function_exists('password_hash')) {
	function password_hash($password,$PASSWORD_DEFAULT=null){
		$salt ='$2a$' . str_pad(8, 2, '0', STR_PAD_LEFT) . '$' .substr(strtr(base64_encode(openssl_random_pseudo_bytes(16)), '+', '.'),0, 22);
		return crypt($password, $salt);
	}
}
if(! function_exists('password_verify')) {
	function password_verify($password,$hash){
		return crypt($password, $hash) == $hash;
	}
}

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="players")
 **/
class Player
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string",unique=true) **/
    protected $login;
    /** @Column(type="string") **/
    protected $password;
    /** @Column(type="boolean") **/
    protected $npc = false;
    /** @Column(type="string",nullable=true) **/
    protected $email;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $lastlogin=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $gold=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $changefactiontoken=1;
	/**
     * @OneToMany(targetEntity="Character", mappedBy="player", indexBy="id")
     * @var Character[]
     **/
    private $characters;
    /**
     * @OneToMany(targetEntity="GameSession", mappedBy="player", indexBy="id")
     * @var GameSession
     **/
    private $gamesessions;
    /** @ManyToOne(targetEntity="Faction", inversedBy="players")
     * @var Faction
     */
    private $faction;
	
	public function __construct()
	{
		$this->characters = new ArrayCollection();
	}
	
    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }
	
	public function setPassword($password)
	{
		$this->password = password_hash($password,PASSWORD_DEFAULT);
	}
	
	public function checkPassword($password)
	{
		return password_verify($password,$this->password) == $this->password;
	}
	
	public function setEmail($email)
	{
		if (filter_var($email,FILTER_VALIDATE_EMAIL))
		{
			$this->email = $email;
		}
	}
	
	public function getFaction()
	{
		return $this->faction;
	}
	
	public function setFaction($faction)
	{
		$this->faction=$faction;
	}
	
	public function changeFaction($faction)
	{
		if (is_null($faction) || $this->getChangeFactionToken() <= 0)
		{
			return false;
		}
		if ($faction->addPlayer($this))
		{
			$currentfaction = $this->getFaction();
			$currentfaction->removePlayer($this);
			$this->setFaction($faction);
			$this->removeChangeFactionToken();
			return true;
		}
		return false;
	}
	
	public function getEmail()
	{
		return $this->email;
	}
	
	public function setLastLogin($time)
	{
		$this->lastlogin = $time;
	}
	
	public function getLastLogin()
	{
		return $this->lastlogin;
	}
	
	public function getCharacters()
	{
		return $this->characters->toArray();
	}

	public function getCharacter($id)
	{
		if (!isset($this->characters[$id]))
		{
			return false;
		}
		else
		{
			return $this->characters[$id];
		}
	}

	public function addCharacter($character)
	{
		$this->characters[$character->getId()] = $character;
	}
	
	public function getGold()
	{
		return $this->gold;
	}
	
	public function addGold($gold)
	{
		$this->gold = $this->gold + $gold;
	}
	
	public function removeGold($gold)
	{
		$this->gold = $this->gold - $gold;
		if ($this->gold < 0)
		{
			$this->gold = 0;
		}
	}
	
	public function getChangeFactionToken()
	{
		return $this->changefactiontoken;
	}
	
	public function addChangeFactionToken($ammount=1)
	{
		$this->changefactiontoken = $this->changefactiontoken + $ammount;
	}
	
	public function removeChangeFactionToken($ammount=1)
	{
		$this->changefactiontoken = $this->changefactiontoken - $ammount;
	}
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'name'=>$this->getLogin(),
			'gold'=>$this->getGold(),
			'factionname'=>$this->getFaction()->getName(),
			'factionid'=>$this->getFaction()->getId(),
			'changefactiontoken'=>$this->getChangeFactionToken()
		);
		return $description;
	}
}
