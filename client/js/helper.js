// highly experimental

var WAITTIMEOUT = 500;
var queue = {};
var mycharacterids = [];

function randomString(length) {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function getWithTrigger(payload,trigger)
{
	requestid = randomString(12);
	payload.token = token;
	sendSyncJSON(payload,requestid);
	queue[requestid]=trigger;
}

function getMyCharacterIDs(token)
{
	var payload = new Object();
	payload.token=token;
	payload.action='listcharacters';
	var trigger = function(d) {
		mycharacterids = [];
		characters = d.characters;
		for (i=0;i<characters.length;i++)
		{
			mycharacterids.push(characters[i].id);
		}
		};
	getWithTrigger(payload,trigger);
}

function getPlayer(token,id,trigger)
{
	var payload = new Object();
	payload.action='getplayer';
	payload.playerid=id;
	getWithTrigger(payload,trigger);
}

function getMission(token,id,trigger)
{
	var payload = new Object();
	payload.action='getmission';
	payload.missionid=id;
	getWithTrigger(payload,trigger);
}

function getCharacter(token,id,trigger)
{
	var payload = new Object();
	payload.action='getcharacter';
	payload.characterid=id;
	getWithTrigger(payload,trigger);
}

function getDifficulties(token,trigger)
{
	var payload = new Object();
	payload.action='listdifficulties';
	getWithTrigger(payload,trigger);
}

function getEvents(token,gsid,trigger)
{
	var payload = new Object();
	payload.action='getevents';
	payload.gamesessionid=gsid;
	getWithTrigger(payload,trigger);
}

function storeDispatcher(data)
{
	requestid = data.syncrequestid;
	setTimeout(queue[requestid](data),WAITTIMEOUT);
}

function placeCharacter(data,stopcond=0)
{
	char = document.getElementById("c"+data.character.x+"-"+data.character.y);
	// sometimes error in local there.
	if (!char && stopcond < 10)
	{
		setTimeout(placeCharacter,100,data,stopcond+1);
	}
	else
	{
		if (data.character.token != null)
		{
			var img = document.createElement("img");
			img.src="tokens/16x16/"+data.character.token+".png";
			char.appendChild(img);
			char.className = 'map_character_wt';
		}
		else
		{
			var img = document.createElement("img");
			img.src="tokens/16x16/default.png";
			char.appendChild(img);
			char.className = 'map_character_wt';
		}
		var img;
		if (data.character.avatar)
		{
			img = $('<img src="avatars/64x64/'+data.character.avatar+'.png"/> ');
		}
		else
		{
			img = $('<img src="avatars/64x64/empty.png"/>');
		}
		$('#charname'+data.character.id).append(img);
		$('#charname'+data.character.id).append(data.character.name);
		$('#rosechar'+data.character.id).on('mouseover',function(){
			charplace = $("#c"+data.character.x+"-"+data.character.y);
			charplace.addClass('over');
			});
		$('#rosechar'+data.character.id).on('mouseout',function(){
		charplace = $("#c"+data.character.x+"-"+data.character.y);
		charplace.removeClass('over');
		});
		if (data.character.turnended)
		{
			$('#characteractions-'+data.character.id).html('');
		}
	}
}

function i18n(key,replacements=null)
{
	var str=dic[key];
	if (str==undefined)
	{
		str = key;
	}
	if (replacements)
	{
		for (key in replacements)
		{
			str = str.replace('%'+key+'%',replacements[key]);
		}
	}
	return str;
}

function displayFactionSymbol()
{
	$('#factionsymbol').html('<img src="img/factions/128x128/'+myprofile.factionname+'.png"/>');
}
