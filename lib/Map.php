<?php

namespace Game;
require_once __DIR__.'/../const.php';

class Map {
	private $map;
	private $dimensions;
	private $startzone;
	private $unwalkable;
	private $ftldrive;
	private $survivesystem;
	private $pilot;
	private $fireproof;
	private $communication;
	private $firezone;
	private $door;
	
	public function __construct($map)
	{
		$map = json_decode($map,true);
		$this->map = $map;
		$this->startzone = $map['start_zone'];
		$this->dimensions = $map['dimensions'];
		if (UNWALKABLE_SYSTEMS)
		{
			$this->unwalkable = array_merge($map['unwalkable'],$map['ftl_drive'],$map['survive_system'],$map['pilot'],$map['communication']);
		}
		else
		{
			$this->unwalkable = $map['unwalkable'];
		}
		
		if (array_key_exists('ftl_drive',$map))
		{
			$this->ftldrive = $map['ftl_drive'];
		}
		else
		{
			$this->ftldrive = array();
		}
		
		if (array_key_exists('survive_system',$map))
		{
			$this->survivesystem = $map['survive_system'];
		}
		else
		{
			$this->survivesystem = array();
		}
		
		if (array_key_exists('communication',$map))
		{
			$this->communication = $map['communication'];
		}
		else
		{
			$this->communication = array();
		}
		
		if (array_key_exists('pilot',$map))
		{
			$this->pilot = $map['pilot'];
		}
		else
		{
			$this->pilot = array();
		}
		
		if (array_key_exists('fire_zone',$map))
		{
			$this->firezone = $map['fire_zone'];
		}
		else
		{
			$this->firezone = array();
		}
		
		if (array_key_exists('door',$map))
		{
			$this->door = $map['door'];
		}
		else
		{
			$this->door = array();
		}
		
		if(array_key_exists('fireproof',$map))
		{
			$this->fireproof = array_merge($map['unwalkable'],$map['fireproof'],$map['door']);
		}
		else
		{
			$this->fireproof = array_merge($map['unwalkable'],$map['door']);
		}
		
	}
	
	public function getMapArray()
	{
		return $this->map;
	}
	
	public function isUnwalkable($x,$y,$z=0)
	{
		$array = $this->unwalkable;
		$walkable = true;
		foreach ($array as $node)
		{
			if ($walkable)
			{
				if ($this->dimensions == 2)
				{
					$walkable = !($node['x'] == $x && $node['y'] == $y);
				}
				else
				{
					$walkable = !($node['x'] == $x && $node['y'] == $y && $node['z'] == $z);
				}
			}
		}
		return !$walkable;
	}
	
	public function isFireproof($x,$y,$z=0)
	{
		$array = $this->fireproof;
		$fireproof = false;
		foreach ($array as $node)
		{
			if (!$fireproof)
			{
				if ($this->dimensions == 2)
				{
					$fireproof = $node['x'] == $x && $node['y'] == $y;
				}
				else
				{
					$fireproof = $node['x'] == $x && $node['y'] == $y && $node['z'] == $z;
				}
			}
		}
		return $fireproof;
	}
	
	public function isOnFire($x,$y,$z=0)
	{
		$array = $this->firezone;
		$onfire = false;
		foreach ($array as $node)
		{
			if (!$onfire)
			{
				if ($this->dimensions == 2)
				{
					$onfire = $node['x'] == $x && $node['y'] == $y;
				}
				else
				{
					$onfire = $node['x'] == $x && $node['y'] == $y && $node['z'] == $z;
				}
			}
		}
		return $onfire;
	}
	
	public function hasFire()
	{
		return count($this->firezone) != 0;
	}
	
	public function hasFireInRange($x,$y,$z,$range)
	{
		return $this->hasSomethingInRange($this->firezone,$x,$y,$z,$range);
	}
	
	public function hasFTLInRange($x,$y,$z,$range)
	{
		return $this->hasSomethingInRange($this->ftldrive,$x,$y,$z,$range);
	}
	
	public function hasSSInRange($x,$y,$z,$range)
	{
		return $this->hasSomethingInRange($this->survivesystem,$x,$y,$z,$range);
	}
	
	public function hasCommunicationInRange($x,$y,$z,$range)
	{
		return $this->hasSomethingInRange($this->communication,$x,$y,$z,$range);
	}
	
	private function hasSomethingInRange($array,$x,$y,$z,$range)
	{
		$found = false;
		$cpt = 0;
		$limit = count($array);
		while(!$found && $cpt < $limit)
		{
			$node = $array[$cpt];
			$found = abs($node['x']-$x) <= $range && abs($node['y']-$y) <= $range;
			if ($this->dimensions == 3)
			{
				$found = $found && abs($node['z']-$z) <= $range;
			}
			$cpt++;
			
		}
		return $found;
	}
}
