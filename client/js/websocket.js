var socket;
var chat;
var map;
var host = "wss://127.0.0.1:9443"; // SET THIS TO YOUR SERVER

function initsocket() {
	
	try {
		socket = new WebSocket(host);
		log('WebSocket - status '+socket.readyState);
		socket.onopen    = function(msg) { 
							   log("Welcome - status "+this.readyState); 
						   };
		socket.onmessage = function(msg) { 
							   log("Received: "+msg.data); 
						   };
		socket.onclose   = function(msg) { 
							   log("Disconnected - status "+this.readyState); 
						   };
	}
	catch(ex){ 
		log(ex); 
	}
}

function initchat() {
	var host = host+"/chat";
	try {
		chat = new WebSocket(host);
		log('WebSocket - status '+chat.readyState);
		chat.onmessage = function(msg) { 
							   log("Received: "+msg.data); 
						   };
		chat.onclose   = function(msg) { 
							   log("Disconnected - status "+this.readyState); 
						   };
	}
	catch(ex){ 
		log(ex); 
	}
}

function initmap() {
	try {
		map = new WebSocket(host);
		log('WebSocket - status '+map.readyState);
		map.onopen    = function(msg) { 
							   log("Welcome - status "+this.readyState); 
						   };
		map.onmessage = function(msg) { 
							   generatemap(msg.data);
						   };
		map.onclose   = function(msg) { 
							   log("Disconnected - status "+this.readyState); 
						   };
	}
	catch(ex){ 
		log(ex); 
	}
}

function send(formname){
	var data = {};
  for (var i = 0, ii = $(formname).length; i < ii; ++i) {
    var input = $(formname)[i];
    if (input.name) {
      data[input.name] = input.value;
    }
  }
  try { 
		socket.send(JSON.stringify(data));  
	} catch(ex) { 
		log(ex); 
	}
	return
}

function quit(){
	if (socket != null) {
		log("Goodbye!");
		socket.close();
		socket=null;
	}
}

function reconnect() {
	quit();
	initsocket();
}

function generatemap(json)
{
	mapobj = JSON.parse(json);
	var tblBody = document.createElement("tbody");
	for (var i = 1; i <= mapobj.size_x; i++) {
    // creates a table row
    var row = document.createElement("tr");
 
    for (var j = 1; j <= mapobj.size_y; j++) {
      // Create a <td> element and a text node, make the text
      // node the contents of the <td>, and put the <td> at
      // the end of the table row
      var cell = document.createElement("td");
      cell.id="c"+i+"-"+j;
      var cellText = document.createTextNode("o");
      cell.appendChild(cellText);
      row.appendChild(cell);
    }
 
    // add the row to the end of the table body
    tblBody.appendChild(row);
	}
	$('formmap').appendChild(tblBody);
	
	for (var i=0; i < mapobj.unwalkable.length; i++)
	{
		element = mapobj.unwalkable[i];
		$("c"+element.x+"-"+element.y).className='unwalkable';
	}
	
	for (var i=0; i < mapobj.ftl_drive.length; i++)
	{
		element = mapobj.ftl_drive[i];
		$("c"+element.x+"-"+element.y).innerText = 'F';
	}
	
	for (var i=0; i < mapobj.survive_system.length; i++)
	{
		element = mapobj.survive_system[i];
		$("c"+element.x+"-"+element.y).innerText = 'S';
	}
	
	for (var i=0; i < mapobj.pilot.length; i++)
	{
		element = mapobj.pilot[i];
		$("c"+element.x+"-"+element.y).innerText = 'P';
	}
}

function sendmap()
{
	var data = {};
  for (var i = 0, ii = $('formmap').length; i < ii; ++i) {
    var input = $('formmap')[i];
    if (input.name) {
      data[input.name] = input.value;
    }
  }
  try { 
		map.send(JSON.stringify(data)); 
		log('Sent: '+JSON.stringify(data)); 
	} catch(ex) { 
		log(ex); 
	}
	return
}

// Utilities
function $(id){ return document.getElementById(id); }
function log(msg){ console.log(msg); }
function onkey(event){ if(event.keyCode==13){ send(); } }
