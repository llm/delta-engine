<?php

/**
 * @Entity @Table(name="characterclasses")
 **/
class CharacterClass
{
    /** @Id @Column(type="integer") **/
    protected $id;
    /** @Column(type="string") **/
    protected $name;
    /** @Column(type="integer",options={"default"=4}) **/
    protected $turndivider=4;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $repair=0;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $speed=1;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $medic=0;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $pilot=0;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $engineering=0;
    /** @Column(type="float",options={"default"=0.5}) **/
    protected $levelcapacitiesmultiplier=0.5;
    /** @Column(type="integer",options={"default"=25}) **/
    protected $maxlevel=25;
    /** @Column(type="float",options={"default"=1}) **/
    protected $levelcostmultiplier=500;
    /** @Column(type="integer",options={"default"=1}) **/
    protected $range=1;
    
    public function __construct($id,$name)
    {
        $this->name = $name;
        $this->id = $id;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    // 1 new turn every x level
    public function getTurnDivider()
    {
		return $this->turndivider;
	}
	
	public function setTurnDivider($divider)
	{
		$this->turndivider = $divider;
	}
	
	public function getNextLevelXP($currentlevel=1)
	{
		return $currentlevel*($currentlevel+1)*$this->levelcostmultiplier;
	}
	
	public function getMaxLevel()
	{
		return $this->maxlevel;
	}
	
	public function getRepair($level=1)
	{
		return round($this->repair*(1*pow($level,$this->levelcapacitiesmultiplier)),0,PHP_ROUND_HALF_DOWN);
	}
	
	public function setRepair($a)
	{
		$this->repair=$a;
	}
	
	public function getPilot($level=1)
	{
		return round($this->pilot*(1*pow($level,$this->levelcapacitiesmultiplier)),0,PHP_ROUND_HALF_DOWN);
	}
	
	public function setPilot($a)
	{
		$this->pilot=$a;
	}
	
	public function getMedic($level=1)
	{
		return round($this->medic*(1*pow($level,$this->levelcapacitiesmultiplier)),0,PHP_ROUND_HALF_DOWN);
	}
	
	public function setMedic($a)
	{
		$this->medic = $a;
	}
	
	public function getEngineering($level=1)
	{
		return round($this->engineering*(1*pow($level,$this->levelcapacitiesmultiplier)),0,PHP_ROUND_HALF_DOWN);
	}
	
	public function setEngineering($a)
	{
		$this->engineering = $a;
	}
	
	public function getSpeed()
	{
		return $this->speed;
	}
	
	public function setSpeed($a)
	{
		$this->speed = $a;
	}
	
	public function getRange()
	{
		return $this->range;
	}
	
	public function setRange($range)
	{
		$this->range = $range;
	}
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'turndivider'=>$this->getTurnDivider(),
			'repair'=>$this->getRepair(1),
			'pilot'=>$this->getPilot(1),
			'medic'=>$this->getMedic(1),
			'engineering'=>$this->getEngineering(1),
			'speed'=>$this->getSpeed(),
			'range'=>$this->getRange()
		);
		return $description;
	}
    
}
