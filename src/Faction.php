<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="faction")
 **/
class Faction
{
	/** @Id @Column(type="integer") @GeneratedValue **/
	protected $id;
	/** @Column(type="string") **/
    protected $name;
	/** @Column(type="integer") **/
    protected $maxplayers;
    /** @Column(type="float",options={"default"=0.5}) **/
    protected $xpsave=0.9;
    /** @Column(type="float",options={"default"=0.5}) **/
    protected $tax=0.5;
    /** @Column(type="float",options={"default"=0.5}) **/
    protected $xptaxonfailure=0.5;
    /** @Column(type="integer",options={"default"=0.5}) **/
    protected $gold=0;
	/**
     * @OneToMany(targetEntity="Player", mappedBy="faction", indexBy="id")
     * @var Player[]
     **/
    private $players;
    /**
     * @OneToMany(targetEntity="Planet", mappedBy="faction", indexBy="id")
     * @var Planet[]
     **/
    private $planets;
    /**
     * @OneToMany(targetEntity="FactionDiplomaticRelation", mappedBy="from")
     * @var FactionDiplomaticRelation[]
     **/
    private $diplomaticrelations;
		
	public function __construct($name,$maxplayers,$xpsave,$tax,$xptax)
	{
		$this->name = $name;
		$this->players = new ArrayCollection();
		$this->planets = new ArrayCollection();
		$this->diplomaticrelations = new ArrayCollection();
		$this->maxplayers = $maxplayers;
		$this->xpsave = $xpsave;
		$this->tax = $tax;
		$this->xptaxonfailure = $xptax;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getPlayers()
	{
		return $this->players->toArray();
	}
	
	public function getGold()
	{
		return $this->gold;
	}
	
	public function addDiplomaticRelation($diplo)
	{
		$this->diplomaticrelations[$diplo->getId()] = $diplo;
	}
	
	public function getDiplomaticRelations()
	{
		return $this->diplomaticrelations;
	}
	
	public function addPlayer($player)
	{
		if ($this->maxplayers != -1 && count($this->getPlayers()) >= $this->maxplayers)
		{
			return false;
		}
		$this->players[$player->getId()] = $player;
		return true;
	}
	
	public function removePlayer($player)
	{
		if (!is_null($player))
		{
			$playerid = $player->getId();
			$play = $this->players[$playerid];
			if (!is_null($play))
			{
				$this->players->remove($playerid);
				return true;
			}
		}
		return false;
	}
	
	public function getMaxPlayers()
	{
		return $this->maxplayers;
	}
	
	public function getXPSave()
	{
		return $this->xpsave;
	}
	
	public function getTaxRate()
	{
		return $this->tax;
	}
	
	public function getGoldAfterTax($gold)
	{
		$tax = round($gold * $this->getTaxRate());
		$this->gold = $this->gold + $tax;
		return $gold - $tax;
	}
	
	public function getXPTaxOnFailure()
	{
		return $this->xptaxonfailure;
	}
	
	public function setXPTaxOnFailure($tax)
	{
		$this->xptaxonfailure=$tax;
	}
	
	public function describe()
	{
		$nbcharacters = 0;
		$players = $this->getPlayers();
		$max = $this->getMaxPlayers();
		if ($max == -1)
		{
			$max=false;
		}
		foreach ($players as $player)
		{
			$nbcharacters += count($player->getCharacters());
		}
		$description = array(
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'xpsave'=>$this->getXPSave(),
			'taxrate'=>$this->getTaxRate(),
			'gold'=>$this->getGold(),
			'nbcharacters'=>$nbcharacters,
			'nbplayers'=>count($players),
			'xplostonfailure'=>$this->getXPTaxOnFailure(),
			'maxplayers'=>$max
		);
		return $description;
	}
	
}
