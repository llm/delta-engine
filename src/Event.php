<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="events")
 **/
class Event
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="integer") **/
    protected $timestamp;
    /** @Column(type="text") **/
    protected $name;
    /** @Column(type="array") **/
    protected $eventvalues;
    /**
     * @ManyToOne(targetEntity="GameSession", inversedBy="events")
     * @var GameSession
     **/
    private $gamesession;
    
    
    public function __construct($gamesession,$name,$values=array())
    {
		$this->gamesession = $gamesession;
		$this->name = $name;
		$this->eventvalues = $values;
		$this->timestamp = time();
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getGameSession()
	{
		return $this->gamesession;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getValues()
	{
		return $this->eventvalues;
	}
	
	public function getTimestamp()
	{
		return $this->timestamp;
	}
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'gamesessionid'=>$this->getGameSession()->getId(),
			'name'=>$this->getName(),
			'values'=>$this->getValues(),
			'timestamp'=>$this->getTimestamp()
		);
		return $description;
	}
	
	
}
