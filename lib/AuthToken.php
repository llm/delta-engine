<?php

require_once __DIR__.'/JWT.php';
require_once __DIR__.'/../bootstrap.php';
require_once __DIR__.'/../const.php';

class AuthToken { 
     
    static function allIPs()
    // Returns the IP address of the client (Used to prevent session cookie hijacking.)
    {
        $ip=""; 
        if (isset($_SERVER['REMOTE_ADDR'])) {$ip=$_SERVER['REMOTE_ADDR'];}
        // Then we use more HTTP headers to prevent session hijacking from users behind the same proxy.
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { $ip=$ip.'_'.$_SERVER['HTTP_X_FORWARDED_FOR']; }
        if (isset($_SERVER['HTTP_CLIENT_IP'])) { $ip=$ip.'_'.$_SERVER['HTTP_CLIENT_IP']; }
        return $ip;
    }
     
    static function checkAuth($login,$password) 
    // Check that user/password is correct.
    {
		global $entityManager;
		$token=array();
		$repository = $entityManager->getRepository('Player');
		$player = $repository->findOneByLogin($login);
        if (!is_null($player) && $player->checkPassword($password))
        {
			$player->setLastLogin(time());
			$entityManager->flush();
            $token['uid'] = sha1(uniqid('',true).'_'.mt_rand()); // generate unique random number (different than phpsessionid)
                                                                    // which can be used to hmac forms and form token (to prevent XSRF)
            $token['ip']=AuthToken::allIPs();                // We store IP address(es) of the client to make sure session is not hijacked.
            $token['username']=$login;
            $token['expires_on']=time()+INACTIVITY_TIMEOUT;  // Set session expiration.
            return JWT::encode($token,PRIVATEKEY);
        }
        else
        {
            return False;
        }
    }
     
    static function checkLogin($jsontoken)
    // Make sure user is logged in. Redirect to login page if not.
    {
		$token = AuthToken::getTokenArray($jsontoken);
        // If session does not exist on server side, or IP address has changed, or session has expired, show login screen.
        if (!isset ($token['uid']) || !$token['uid'] || $token['ip']!=AuthToken::allIPs() || time()>=$token['expires_on'])
        {
            return false;
        }
        
        return true;
    }
    
    static function getTokenArray($jsontoken)
    {
		try {
		$token = JWT::decode($jsontoken,PRIVATEKEY);
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
			return false;
		}
		return (array)$token;
	}
    
    static function regenerateToken($jsontoken)
    {
		if (AuthToken::checkLogin($jsontoken))
		{
			$token = (array)JWT::decode($jsontoken,PRIVATEKEY);
			$token['expires_on']=time()+INACTIVITY_TIMEOUT;
			return JWT::encode($token,PRIVATEKEY);
		}
		return false;
	}
	
	static function getUsername($jsontoken)
	{
		$token = AuthToken::getTokenArray($jsontoken);
		return $token['username'];
	}
}
