<?php
// Technical global
define('VERSION','dev 0.1');
define('PRIVATEKEY','private_key'); // CHANGE IT
define('INACTIVITY_TIMEOUT',3600);

// Player

define('LOGIN_MIN_CHAR',3);

// Faction

define('FACTION_DEFAULT_ID',1);

define('MIN_RELATION',0);
define('MAX_RELATION',1000);
define('RELATION_WAR',50);
define('RELATION_HATE',150);
define('RELATION_COLD',400);
define('RELATION_WARM',600);
define('RELATION_ALLY',850);

define('DEFAULT_RELATION',500);

define('RELATION_WIN_BONUS_PER_DIFFICULTY_LEVEL',1);
define('RELATION_FAIL_BONUS_PER_DIFFICULTY_LEVEL',2);

// Character

define('MAX_CHARACTERS',10);
define('MIN_CHAR_CHARACTER_NAME',3);
define('CHARACTER_MAX_HP',100);

// Mission

// Game Session

define('MIN_TURN',10);
define('DEFAULT_MAX_TURN',15);
// DIFFICULTIES : Recrue, Easy, Normal, Challenging, Hard, Elite
define('MIN_DIFFICULTY',1); // EZ PZ
define('MAX_DIFFICULTY',6); // Nice to meet you, Satan
define('MIN_DIFFICULTY_FOR_GAME_OVER_ON_ZERO',4);
define('MIN_DIFFICULTY_FOR_NOT_ALL_SYSTEMS_ON_ZERO_LAST_ROUND',3);
define('MIN_DIFFICULTY_FOR_UNREPAIRABLE_ON_ZERO',2);
define('MAX_FIRE_INTENSITY',3);

define('UNWALKABLE_SYSTEMS',true);

define('XP_PER_DIFFICULTY_LEVEL',50);
define('GOLD_REWARD_PER_DIFFICULTY_LEVEL',0.5);
define('CHANCE_OF_EVENT_PER_DIFFICULTY_LEVEL',13);
define('MAX_DAMAGE_PER_DIFFICULTY_LEVEL',2);
define('OXYGEN_CONSUMPTION_PER_DIFFICULTY_LEVEL',0.5);
define('CHANCE_OF_FIRE',10); // on 100

define('OXYGEN_LEVEL_WARNING',50);
define('OXYGEN_LEVEL_CRITICAL',20);
define('OXYGEN_NEW_PER_TURN',5);

define('OXYGEN_DAMAGE_WARNING',2);
define('OXYGEN_DAMAGE_CRITICAL',10);

define('PREPARATION_TURN',0);

// Chat

define('CHAT_CHANNEL_PREFIX','gamesession-');
define('CHAT_MAX_HISTORY_LINES',50);
define('CHAT_HISTORY_PATH',__DIR__.'/chat/history');
