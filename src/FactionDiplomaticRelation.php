<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="factiondiplomaticrelation")
 **/
class FactionDiplomaticRelation
{
	/** @Id @Column(type="integer") @GeneratedValue **/
	protected $id;
	/**
     * @ManyToOne(targetEntity="Faction", inversedBy="diplomaticrelations")
     * @var Faction
     **/
    private $from;
    /**
     * @OneToOne(targetEntity="Faction")
     * @var Faction[]
     **/
    private $to;
    /** @Column(type="integer")**/
	protected $relation=500;
		
	public function __construct($from,$to,$relation=500)
	{
		$this->from = $from;
		$this->to = $to;
		$this->relation = $relation;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getFrom()
	{
		return $this->from;
	}
	
	public function getTo()
	{
		return $this->to;
	}
	
	public function getRelation()
	{
		return $this->relation;
	}
	
	public function increaseRelation($ammount)
	{
		$this->relation = $this->relation + $ammount;
		if ($this->relation > MAX_RELATION)
		{
			$this->relation = MAX_RELATION;
		}
	}
	
	public function decreaseRelation($ammount)
	{
		$this->relation = $this->relation - $ammount;
		if ($this->relation < MIN_RELATION)
		{
			$this->relation = MIN_RELATION;
		}
	}
	
	public function getRelationStatus()
	{
		$rel = $this->getRelation();
		$status = 'relation.neutral';
		if ($rel <= RELATION_WAR)
		{
			$status = 'relation.war';
		}
		elseif($rel <= RELATION_HATE)
		{
			$status = 'relation.hate';
		}
		elseif($rel <= RELATION_COLD)
		{
			$status = 'relation.cold';
		}
		elseif ($rel >= RELATION_ALLY)
		{
			$status = 'relation.ally';
		}
		elseif ($rel >= RELATION_WARM)
		{
			$stauts = 'reltion.warm';
		}
		return $status;
	}
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'from'=>$this->getFrom()->describe(),
			'to'=>$this->getTo()->describe(),
			'relation'=>$this->getRelation(),
			'relationstatus'=>$this->getRelationStatus()
			
		);
		return $description;
	}
	
}
