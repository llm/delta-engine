<?php

/* types :
 * head
 * suit
 * boots
 * mainarm
 * weakarm
 * mainhand
 * weakhand
 * chest
 */

/**
 * @Entity @Table(name="equipments")
 **/
class Equipment
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text", unique=true) **/
    protected $name;
    /** @Column(type="text") **/
    protected $type;
    /** @Column(type="integer") **/
    protected $price=10000;
    /** @Column(type="boolean") **/
    protected $buildable=true;
    /** @Column(type="integer") **/
    protected $repairbonus;
    /** @Column(type="integer") **/
    protected $firefightbonus;
    /** @Column(type="float") **/
    protected $xpbonus;
    /** @Column(type="float") **/
    protected $goldbonus;
    /** @Column(type="integer") **/
    protected $oxygenreserve;
    
    public function __construct($name,$type,$price,$repairbonus,$firefightbonus,$xpbonus,$goldbonus,$oxygenreserve)
    {
		$this->name = $name;
		$this->type = $type;
		$this->price = $price;
		$this->repairbonus = $repairbonus;
		$this->firefightbonus = $firefightbonus;
		$this->xpbonus = $xpbonus;
		$this->goldbonus = $goldbonus;
		$this->oxygenreserve = $oxygenreserve;
	}

    public function getId()
    {
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getBuildable()
	{
		return $this->buildable;
	}

	public function isBuildable()
	{
		return $this->getBuildable();
	}

	public function setBuildable($buildable)
	{
		$this->buildable = $buildable;
	}

	public function setPrice($price)
	{
		$this->price=$price;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getFormatedPrice($decPoint='.',$thousandsSep=' ')
	{
		return Tools::getFormatedNumber($this->getPrice());
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function setType($type)
	{
		$this->type=$type;
	}
	
	public function getRepairBonus()
	{
		return $this->repairbonus;
	}
	
	public function getFireFightBonus()
	{
		return $this->firefightbonus;
	}
	
	public function getXPBonusRate()
	{
		return $this->xpbonus;
	}
	
	public function getGoldBonusRate()
	{
		return $this->goldbonus;
	}
	
	public function getOxygenReserve()
	{
		return $this->oxygenreserve;
	}
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'price'=>$this->getPrice(),
			'type'=>$this->getType(),
			'buildable'=>$this->getBuildable(),
			'repairbonus'=>$this->getRepairBonus(),
			'firefightbonus'=>$this->getFireFightBonus(),
			'xpbonusrate'=>$this->getXPBonusRate(),
			'goldbonusrate'=>$this->getGoldBonusRate(),
			'oxygenreserve'=>$this->getOxygenReserve()
		);
		return $description;
	}
}
