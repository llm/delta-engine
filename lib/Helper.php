<?php

use Tools\Tools;

require_once __DIR__.'/../bootstrap.php';
require_once __DIR__.'/Tools.php';
require_once __DIR__.'/AuthToken.php';

class Helper {
	static function register($login,$password,$email)
	{
		global $entityManager;
		$faction = $entityManager->getRepository('Faction')->findOneById(FACTION_DEFAULT_ID);
		$login = Tools::sanitize($login);
		if (Helper::loginExists($login))
		{
			return false;
		}
		$player = new Player();
		$player->setLogin($login);
		$player->setPassword($password);
		if ($email !== '')
		{
			$player->setEmail($email);
		}
		$player->setFaction($faction);
		$entityManager->persist($player);

		$entityManager->flush();
		return true;
	}
	
	static function login($login,$password)
	{
		return AuthToken::checkAuth($login,$password);
	}
	
	static function regenerateToken($token)
	{
		return AuthToken::regenerateToken($token);
	}
	
	static function loginExists($login)
	{
		global $entityManager;
		$test = $entityManager->getRepository('Player')->findOneByLogin($login);
		return !is_null($test);
	}
	
	static function checkToken($token)
	{
		return AuthToken::checkLogin($token);
	}
	
	static function getPlayer($payload,$generic=true)
	{
		global $entityManager;
		if ($generic)
		{
			$playerid = Tools::arrayGetInt($payload,'playerid');
			$player = $entityManager->getRepository('Player')->findOneById($playerid);
			if (!is_null($player))
			{
				return $player->describe();
			}
			else
			{
				return null;
			}
		}
		else
		{
			$login = AuthToken::getUsername($payload);
			return $entityManager->getRepository('Player')->findOneByLogin($login);
		}
	}
	
	static function createCharacter($player,$json)
	{	
		if (count($player->getCharacters()) >= MAX_CHARACTERS)
		{
			return false;
		}
		global $entityManager;
		$classid = Tools::arrayGetString($json,'classid');
		$name = Tools::arrayGetString($json,'name');
		$gender = Tools::arrayGet($json,'gender',50);
		if (strlen($name) < MIN_CHAR_CHARACTER_NAME)
		{
			return false;
		}
		$class = $entityManager->getRepository('CharacterClass')->findOneById($classid);
		if (!is_null($class))
		{
			$character = new Character($player,$name,$class);
			$character->setGender($gender);
			
			$entityManager->persist($character);
			$player->addCharacter($character);
			$entityManager->flush();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	static function getCharacter($player,$json)
	{
		global $entityManager;
		$id = Tools::arrayGetInt($json,'characterid');
		$character = $entityManager->getRepository('Character')->findOneById($id);
		if (!is_null($player) && $character->getPlayer()->getId() == $player->getId())
		{
			return $character->getProfile();
		}
		else
		{
			return $character->getLimitedProfile();
		}
	}
	
	public function listCharacters($player)
	{
		$characters = $player->getCharacters();
		$result = array();
		foreach ($characters as $character)
		{
			array_push($result,$character->getProfile());
		}
		return $result;
	}
	
	static function listCharacterClasses()
	{
		global $entityManager;
		$classes = $entityManager->getRepository('CharacterClass')->findAll();
		$result = array();
		foreach ($classes as $class)
		{
			array_push($result,$class->describe());
		}
		return $result;
	}
	
	static function listEquipments()
	{
		global $entityManager;
		$equipments = $entityManager->getRepository('Equipment')->findAll();
		$result = array();
		foreach ($equipments as $equipment)
		{
			array_push($result,$equipment->describe());
		}
		return $result;
	}
	
	static function listMissionTypes()
	{
		global $entityManager;
		$missiontypes = $entityManager->getRepository('MissionType')->findAll();
		$result = array();
		foreach ($missiontypes as $missiontype)
		{
			if ($missiontype->getBuildable())
			{
				array_push($result,$missiontype->describe());
			}
		}
		return $result;
	}
	
	static function listGameSessions($player,$json)
	{
		global $entityManager;
		$gamesessions = $entityManager->getRepository('GameSession')->findAll();
		$result = array();
		foreach ($gamesessions as $gs)
		{
			if ($player->getFaction()->getId() == $gs->getGameMaster()->getFaction()->getId())
			{
				$playable = $gs->isPlayable();
				if (Tools::arrayGet($json,'playableonly',false) && $playable)
				{
					array_push($result,$gs->describe());
				}
				elseif (!Tools::arrayGet($json,'playableonly',false))
				{
					array_push($result,$gs->describe());
				}
			}
		}
		return $result;
	}
	
	static function listPlayers()
	{
		global $entityManager;
		$players = $entityManager->getRepository('Player')->findAll();
		$result = array();
		foreach ($players as $player)
		{
			array_push($result,$player->describe());
		}
		return $result;
	}
	
	static function listFactions()
	{
		global $entityManager;
		$factions = $entityManager->getRepository('Faction')->findAll();
		$result = array();
		foreach ($factions as $faction)
		{
			array_push($result,$faction->describe());
		}
		return $result;
	}
	
	static function createGameSession($player,$json)
	{
		global $entityManager;
		$name = Tools::arrayGetString($json,'name');
		if (count($entityManager->getRepository('Gamesession')->findByName($name)) != 0)
		{
			return false;
		}
		$maxplayers = Tools::arrayGetInt($json,'maxplayers');
		$missionid = Tools::arrayGetInt($json,'missiontypeid');
		$maxturn = Tools::arrayGetInt($json,'maxturn');
		$difficulty = Tools::arrayGetInt($json,'difficulty');
		$characterid = Tools::arrayGetInt($json,'characterid');
		$missionname = Tools::arrayGetString($json,'missionname');
		$factionid = Tools::arrayGet($json,'factionid',null);
		$character=null;
		$faction=null;
		if (empty($name) || $name == '' || !is_numeric($missionid) || !is_numeric($characterid) || !is_numeric($maxturn) || !is_numeric($difficulty))
		{
			return false;
		}
		
		if ($difficulty < MIN_DIFFICULTY || $difficulty > MAX_DIFFICULTY)
		{
			return false;
		}
		
		if ($characterid > 0)
		{
			$char = $entityManager->getRepository('Character')->findOneById($characterid);
			if (is_null($char))
			{
				return false;
			}
			if ($char->getPlayer()->getId() == $player->getId())
			{
				if (is_null($char->getGameSession()))
				{
					$character = $char;
				}
			}
		
		}
		$missiontype = $entityManager->getRepository('MissionType')->findOneById($missionid);
		if (is_null($missiontype))
		{
			return false;
		}
		if ($maxplayers > $missiontype->getMaxPlayers() || $maxplayers == 0)
		{
			$maxplayers = $missiontype->getMaxPlayers();
		}
		$mission = new Mission($missiontype);
		if ($missionname == '')
		{
			$missionname = $name;
		}
		$mission->setName($missionname);
		$entityManager->persist($mission);
		
		if ($maxturn == 0)
		{
			$maxturn = DEFAULT_MAX_TURN;
		}
		
		if ($maxturn < MIN_TURN)
		{
			$maxturn = MIN_TURN;
		}
		
		if (!is_null($factionid) && $factionid != $player->getFaction()->getId())
		{
			$faction = $entityManager->getRepository('Faction')->findOneById($factionid);
		}
		
		$gamesession = new GameSession($player,$name,$maxplayers,$mission,$maxturn,$faction);
		$gamesession->setDifficulty($difficulty);
		if (!is_null($character))
		{
			$gamesession->addCharacter($character);
		}
		
		$entityManager->persist($gamesession);
		$entityManager->flush();
		return true;
	}
	
	static function getObjectCharacter($characterid)
	{
		global $entityManager;
		$character;
		
		if (!is_numeric($characterid))
		{
			return false;
		}
		
		if ($characterid > 0)
		{
			$char = $entityManager->getRepository('Character')->findOneById($characterid);
			if (is_null($char))
			{
				return false;
			}
			$character = $char;
		
		}
		
		return $character;
	}
	
	static function joinGameSession($player,$json)
	{
		global $entityManager;
		$characterid = Tools::arrayGetInt($json,'characterid');
		$gamesessionid = Tools::arrayGetString($json,'gamesessionid');
		$character = Helper::getObjectCharacter($characterid);
		$gamesession = Helper::getObjectGameSession($gamesessionid);
		
		if (!$gamesession || $gamesession->isLocked())
		{
			return false;
		}
		
		if (!$character)
		{
			return false;
		}
		if ($character->getPlayer()->getId() == $player->getId())
		{
			if (!is_null($character->getGameSession()))
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
		if (!$gamesession->isPlayable())
		{
			return false;
		}
		
		$gamesession->addCharacter($character);
		$entityManager->flush();
		return true;
	}
	
	static function getMissionType($json,$getobject=false)
	{
		global $entityManager;
		$missionid = Tools::arrayGetInt($json,'missiontypeid');
		$missiontype = $entityManager->getRepository('missionType')->findOneById($missionid);
		if (!is_null($missiontype))
		{
			if ($getobject)
				return $missiontype;
			
			return $missiontype->describe();
		}
		else
		{
			return null;
		}
	}
	
	static function getMission($json,$getobject=false)
	{
		global $entityManager;
		$missionid = Tools::arrayGetInt($json,'missionid');
		$mission = $entityManager->getRepository('Mission')->findOneById($missionid);
		if (!is_null($mission))
		{
			if ($getobject)
				return $mission;
			
			return $mission->describe();
		}
		else
		{
			return null;
		}
	}
	
	static function getGameSession($json)
	{
		global $entityManager;
		$gamesessionid = Tools::arrayGetInt($json,'gamesessionid');
		$gamesession = $entityManager->getRepository('GameSession')->findOneById($gamesessionid);
		if (!is_null($gamesession))
		{
			return $gamesession->describe();
		}
		else
		{
			return null;
		}
	}
	
	static function getEvents($json)
	{
		global $entityManager;
		$gamesessionid = Tools::arrayGetInt($json,'gamesessionid');
		$gamesession = $entityManager->getRepository('GameSession')->findOneById($gamesessionid);
		$events = $gamesession->getEvents();
		$return = array();
		foreach ($events as $event)
		{
			array_push($return,$event->describe());
		}
		return $return;
	}
	
	static function moveTo($json)
	{
		global $entityManager;
		$character = $entityManager->getRepository('Character')->findOneById($json['characterid']);
		if (!is_null($character->getGameSession()))
		{
			$x = $json['x'];
			$y = $json['y'];
			$z = 0;
			if (array_key_exists('z',$json))
			{
				$z = $json['z'];
			}
			$res = $character->moveToCoordonates($x,$y,$z);
			$entityManager->flush();
			return $res;
		}
	}
	
	static function getObjectGameSession($gamesessionid)
	{
		global $entityManager;
		if ($gamesessionid > 0)
		{
			$gs = $entityManager->getRepository('GameSession')->findOneById($gamesessionid);
			if (is_null($gs))
			{
				return false;
			}
			return $gs;
		}
	}
	
	static function endTurn($player,$json)
	{
		global $entityManager;
		$characterid = Tools::arrayGetInt($json,'characterid');
		$gamesessionid = Tools::arrayGetString($json,'gamesessionid');
		$character = Helper::getObjectCharacter($characterid);
		$gamesession = Helper::getObjectGameSession($gamesessionid);
		
		if (!$character || !$gamesession)
		{
			return false;
		}
		if ($character->getPlayer()->getId() == $player->getId())
		{
			if (is_null($character->getGameSession()))
			{
				return false;
			}
			$character->endTurn();
			$entityManager->flush();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	static function deltaMoveTo($json)
	{
		global $entityManager;
		$character = $entityManager->getRepository('Character')->findOneById($json['characterid']);
		if (!is_null($character->getGameSession()))
		{
			$dx = $json['dx'];
			$dy = $json['dy'];
			$dz = 0;
			if (array_key_exists('dz',$json))
			{
				$dz = $json['dz'];
			}
			$res = $character->move($dx,$dy,$dz);
			$entityManager->flush();
			return $res;
		}
	}
	
	static function getReward($player,$json)
	{
		global $entityManager;
		$characterid = Tools::arrayGetInt($json,'characterid');
		$character = Helper::getObjectCharacter($characterid);
		$gamesession = $character->getGameSession();
		$success = false;
		if ($character->getPlayer()->getId() != $player->getId() || !$gamesession->isEnded())
		{
			return false;
		}
		if (!$gamesession->isGameOver())
		{
			$character->getReward();
			$success = true;
		}
		else
		{
			$xptaxrate = $character->getPlayer()->getFaction()->getXPTaxOnFailure();
			$xptosubstract = round($character->getXp() * $xptaxrate);
			$character->removeXP($xptosubstract);
			$gamesession->removeCharacter($character);
		}
		if (count($gamesession->getCharacters()) == 0)
		{
			if (!is_null($gamesession->getWithFaction()))
			{
				$forfaction = $gamesession->getWithFaction();
				$relation = $entityManager->getRepository('FactionDiplomaticRelation')->findOneBy(array('to'=>$gamesession->getGameMaster()->getFaction(),'from'=>$forfaction));
				if (is_null($relation))
				{
					$relation = new FactionDiplomaticRelation($forfaction,$gamesession->getGameMaster()->getFaction(),DEFAULT_RELATION);
					$entityManager->persist($relation);
					$forfaction->addDiplomaticRelation($relation);
				}
				if ($success)
				{
					$relation->increaseRelation(RELATION_WIN_BONUS_PER_DIFFICULTY_LEVEL);
				}
				else
				{
					$relation->decreaseRelation(RELATION_FAIL_BONUS_PER_DIFFICULTY_LEVEL);
				}
			}
			file_put_contents(CHAT_HISTORY_PATH.'/'.$gamesession->getChatChannel(),'',LOCK_EX);
			$entityManager->remove($gamesession);
		}
		$entityManager->flush();
		return is_null($character->getGameSession());
	}
	
	static function getFactionRelations($json)
	{
		global $entityManager;
		$factionid = Tools::arrayGetInt($json,'factionid');
		$faction = $entityManager->getRepository('Faction')->findOneById($factionid);
		$relations = $faction->getDiplomaticRelations();
		$result = array();
		foreach($relations as $relation)
		{
			array_push($result,$relation->describe());
		}
		return $result;
	}
	
	static function act($player,$json)
	{
		global $entityManager;
		$characterid = Tools::arrayGetInt($json,'characterid');
		$character = Helper::getObjectCharacter($characterid);
		if (is_null($character) || $character->getPlayer()->getId() != $player->getId())
		{
			return false;
		}
		$gamesession = $character->getGameSession();
		$action = Tools::arrayGetString($json,'characteraction');
		$act=true;
		switch ($action)
		{
			case 'repair':
				$character->repair();
			break;
			case 'extinguish':
				$character->extinguishFire();
			break;
			case 'build':
				
			break;
			case 'pilot':
				
			break;
			case 'heal':
				
			break;
			case 'fire':
				
			break;
			case 'repairhull':
				
			break;
			default:
				$act=false;
			break;
		}
		$entityManager->flush();
		return $act;
	}
	
	static function updateCharacter($player,$json)
	{
		global $entityManager;
		$characterid = Tools::arrayGetInt($json,'characterid');
		$character = Helper::getObjectCharacter($characterid);
		if (is_null($character) || $character->getPlayer()->getId() != $player->getId())
		{
			return false;
		}
		$name = Tools::arrayGetString($json,'name');
		$gender = Tools::arrayGet($json,'gender',null);
		$bot = Tools::arrayGet($json,'bot',false);
		$avatar = Tools::arrayGetString($json,'avatar');
		
		if (!empty($name) && strlen($name) >= MIN_CHAR_CHARACTER_NAME)
		{
			$character->setName($name);
		}
		if (!is_null($gender))
		{
			$character->setGender($gender);
		}
		if ($character->isBot() != $bot)
		{
			$character->toggleBot();
		}
		if (!empty($avatar))
		{
			$character->setAvatar($avatar);
		}
		$entityManager->flush();
		return true;
	}
	
	static function createEvent($gamesession,$name,$values=array())
	{
		global $entityManager;
		$event = new Event($gamesession,$name,$values);
		$entityManager->persist($event);
		$entityManager->flush();
		return $event;
	}
	
	static function changeFaction($player,$json)
	{
		if ($player->getChangeFactionToken() <= 0)
		{
			return false;
		}
		global $entityManager;
		$factionid = Tools::arrayGetInt($json,'factionid');
		$faction = $entityManager->getRepository('Faction')->findOneById($factionid);
		$player->changeFaction($faction);
		$entityManager->flush();
		return true;
	}
	
}
