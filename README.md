Delta Engine est le moteur de jeu de [FTL Nations][1]. Le principe est d'offrir un API de jeu (websocket) permettant de créer les interfaces de jeu que vous souhaitez.

Une interface en HTML5 est proposée, mais elle n'est qu'un exemple de possibilité.

Pour plus d'informations, consultez [le wiki][2].

[1]: https://ftlnations.online/
[2]: https://framagit.org/llm/delta-engine/wikis/home