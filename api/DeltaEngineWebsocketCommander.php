<?php

class DeltaEngineWebsocketCommander
{
	static function chat(&$ws,$action,&$user,$json,&$reply,$player)
	{
		$chatId;
		if (array_key_exists('chatId',$json))
		{
			$chatId=$json['chatId'];
		}
		else
		{
			$chatId = $user->getChatId();
		}
		$chatId = str_replace('/','',$chatId);
		if ($action=='post')
		{
			$msg = array('username'=>$player->getLogin(),'message'=>htmlentities($json['msg']));
			$ws->sendToAllChatUsers($chatId,json_encode($msg));
			
			$content = serialize($msg)."\n";
			file_put_contents(CHAT_HISTORY_PATH.'/'.$chatId,$content,FILE_APPEND | LOCK_EX);
		}
		elseif ($action=='setchatid')
		{
			$chatId=$json['chatId'];
			$user->setChatId($chatId);
		}
		elseif ($action=='getchatid')
		{
			$reply=array('action'=>$action,'result'=>$user->getChatId());
		}
		elseif ($action=='gethistory')
		{
			$file = CHAT_HISTORY_PATH.'/'.$chatId;
			$handle = fopen($file, "r");
			if (!$handle)
			{
				return false;
			}
		    $linecounter = CHAT_MAX_HISTORY_LINES;
		    $pos = -2;
		    $beginning = false;
		    $text = array();
		    while ($linecounter > 0) {
				 $t = " ";
				 while ($t != "\n") {
					if(fseek($handle, $pos, SEEK_END) == -1) {
						$beginning = true; break; }
					$t = fgetc($handle);
					$pos --;
				 }
				 $linecounter --;
				 if($beginning) rewind($handle);
				 $text[CHAT_MAX_HISTORY_LINES-$linecounter-1] = unserialize(fgets($handle));
				 if($beginning) break;
		   }
		   fclose ($handle);
		   $reply=array('action'=>$action,'result'=>array_reverse($text));
		}
	}
}
