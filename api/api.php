#!/usr/local/bin/php
<?php
 
require_once(__DIR__.'/lib/websockets.php');
require_once(__DIR__.'/DeltaEngineWebsocket.php');
require_once(__DIR__.'/../bootstrap.php');
 
$game = $entityManager->getRepository('Game')->find(1);
 
$host = '0.0.0.0';
$port = 9000;
 
$server = new DeltaEngineWebsocket($host , $port );
while (true)
{
	try {
	  $server->run();
	}
	catch (Exception $e) {
	  $server->stdout($e->getMessage());
	}
}
