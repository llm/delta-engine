<?php
// NOTE: Smarty has a capital 'S'
require_once(__DIR__.'/bootstrap.php');
require_once(__DIR__.'/lib/i18n.php');
require_once(__DIR__.'/const.php');
$smarty = new Smarty();

$smarty->setTemplateDir('tpl');
$smarty->setCompileDir('tmp');
$smarty->setConfigDir('conf');
$smarty->setCacheDir('cache');

$smarty->assign('title','FTL Escape');

$i18n = new I18n();
$i18n->autoSetLang();

//** un-comment the following line to show the debug console
//$smarty->debugging = true;

$isInit = count($entityManager->getRepository('Game')->findAll()) == 1;

$smarty->assign('init',$isInit);
$smarty->assign('version',VERSION);

$canSignup = false;
$game = $entityManager->getRepository('Game')->find(1);
if (isset($game))
{
	$canSignup = $game->getSignup();
	$smarty->assign('canSignup',$canSignup);
}

$hasMessage = false;
if (array_key_exists('status',$_GET))
{
	$status = $_GET['status'];
	switch($status)
	{
		case 1: $smarty->assign('msg',$i18n->getText('msg.account.created')); break;
		case 2: $smarty->assign('msg',$i18n->getText('msg.login.too.short')); break;
		case 3: $smarty->assign('msg',$i18n->getText('msg.passwords.mismatch')); break;
		case 4: $smarty->assign('msg',$i18n->getText('msg.login.exists')); break;
		case 5: $smarty->assign('msg',$i18n->getText('msg.invalid.email')); break;
		case 7: $smarty->assign('msg',$i18n->getText('msg.cannot.signup')); break;
		default: $smarty->assign('msg',$i18n->getText('msg.unknown.error'));
	}
	$hasMessage=true;
}
if (array_key_exists('errorno',$_GET))
{
	$errorno = $_GET['errorno'];
	switch($errorno)
	{
		case 1: $smarty->assign('msg',$i18n->getText('msg.invalid.login')); break;
		case 2: $smarty->assign('msg',$i18n->getText('msg.invalid.login')); break;
		case 3: $smarty->assign('msg',$i18n->getText('msg.general.login.error')); break;
		default: $smarty->assign('msg',$i18n->getText('msg.unknown.error'));
	}
	$hasMessage=true;
}
$smarty->assign('hasMessage',$hasMessage);
$smarty->assign('txt_synopsis', $i18n->getText('txt.synopsis'));
$smarty->assign('lbl_signup', $i18n->getText('lbl.signup'));
$smarty->assign('lbl_login', $i18n->getText('lbl.login'));
$smarty->assign('lbl_password', $i18n->getText('lbl.password'));
$smarty->assign('lbl_password_again', $i18n->getText('lbl.password.again'));
$smarty->assign('lbl_connect', $i18n->getText('lbl.connect'));
$smarty->assign('lbl_email',$i18n->getText('lbl.email'));
$smarty->assign('lbl_cannot_signup',$i18n->getText('lbl.cannot.signup'));

$smarty->display('index.tpl');
