

function display(action,data){
	var content;
    
	switch(action)
	{
		case 'general':
			$.get('inc/general.inc.html', function(template) {
			content = Mustache.render(template, data);
			$('#content').html(content);
		  },null,'html');
		break;
		case 'characters':
			$.get('inc/characters.inc.html', function(template) {
			content = Mustache.render(template, data);
			$('#content').html(content);
		  },null,'html');
		break;
		case 'character':
			newdata = {};
			characterclassarray=characterclasses[data['character']['classid']];
			newdata['character'] = data['character'];
			newdata['characterclass'] = characterclassarray;
			
			$.get('inc/character.inc.html', function(template) {
			content = Mustache.render(template, newdata);
			$('#content').html(content);
		  },null,'html');
		  
		break;
		case 'gamesessions':
			$.get('inc/gamesessions.inc.html', function(template) {
			content = Mustache.render(template, data);
			$('#content').html(content);
		  },null,'html');
		break;
		case 'gamesession':
			newdata = {};
			newdata['gamesession'] = data['gamesession'];
			$.get('inc/gamesession.inc.html', function(template) {
			content = Mustache.render(template, newdata);
			$('#content').html(content);
			changeChat('gamesession-'+newdata['gamesession'].id);
		  },null,'html');
		break;
		case 'players':
			$.get('inc/players.inc.html', function(template) {
			content = Mustache.render(template, data);
			$('#content').html(content);
		  },null,'html');
		break;
		case 'factions':
			$.get('inc/factions.inc.html', function(template) {
			content = Mustache.render(template, data);
			$('#content').html(content);
		  },null,'html');
		break;
		case 'menu':
			$.get('inc/menu.inc.html', function(template) {
			content = Mustache.render(template, data);
			$('#menu').html(content);
		  },null,'html');
		break;
	}
	
}
