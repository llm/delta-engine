function get(id){ return document.getElementById(id); }
function log(msg){ console.log(msg); }
function onkey(event){ if(event.keyCode==13){ send(); } }

function App(host,contentDOM,chatDOM){
	this.host = host;
	this.chatDOM = chatDOM;
	this.contentDOM = contentDOM;
}

var chat;
var socket;
var token;
var currentdisplay;
var debug = true;

// "Static" game content
var missiontypes='';
var factions='';
var characterclasses=[];

// less "static" but still...
var characters='';
var players='';
var myprofile='';
	
function initsocket(host)
{	
	try {
		socket = new WebSocket(host);
		console.debug('WebSocket - status '+socket.readyState);
		socket.onopen    = function(msg) { 
							   console.debug("Welcome - status "+socket.readyState); 
						   };
		socket.onmessage = function(msg) { 
							   console.debug("Received: "+msg.data);
							   data = JSON.parse(msg.data);
							   dispatcher(data);
						   };
		socket.onclose   = function(msg) { 
							   console.debug('Disconnected (code : '+msg.code+' / reason : '+msg.reason+')');
							   showWarning('Déconnexion du serveur (code '+msg.code+'). '); 
						   };
		socket.onerror   = function(msg) {
								console.error('Une erreur s\'est produite lors du contact avec le serveur : ',msg);
								showWarning('Une erreur s\'est produite lors du contact avec le serveur. Veuillez réessayer ultérieurement. ');
							};
	}
	catch(ex){ 
		console.error(ex); 
	}
}
	
function initchat(host,chatDOM) {
	var host = host+"/chat";
	try {
		chat = new WebSocket(host);
		console.debug('WebSocket - status '+chat.readyState);
		chat.onopen    = function(msg) { 
								   console.debug("Welcome - status "+this.readyState);
							   };
		chat.onmessage = function(msg) { 
								data = JSON.parse(msg.data);
								switch(data.action)
								{
									case 'gethistory':
										var history = data.msg.result;
										var hlen = history.length;
										for (i=0;i<hlen;i++)
										{
											if (history[i]['username'])
											{
												chatDOM.innerHTML = chatDOM.innerHTML + "<p><strong>"+ history[i]['username'] + '</strong> : ' + history[i]['message'] +"<p>";
											}
										}
									break;
									default:
										chatDOM.innerHTML = chatDOM.innerHTML + "<p><strong>"+ data['username'] + '</strong> : ' + data['message'] +"<p>";
									break;
								}
						   };
		chat.onclose   = function(msg) { 
							   console.debug("Disconnected - status "+this.readyState);
							   showWarning('Déconnecté du serveur de chat. ');
						   };
	}
	catch(ex){ 
		console.error(ex); 
	}
}

function initmap(host) {
	var host = host+"/map";
	try {
		map = new WebSocket(host);
		console.debug('WebSocket - status '+map.readyState);
		map.onopen    = function(msg) { 
							   console.debug("Welcome - status "+this.readyState); 
						   };
		map.onmessage = function(msg) { 
							   generatemap(msg.data);
						   };
		map.onclose   = function(msg) { 
							   console.debug("Disconnected - status "+this.readyState);
							   showWarning('Déconnecté du serveur de cartes. ');
						   };
	}
	catch(ex){ 
		console.error(ex); 
	}
	$("msg").focus();
}

function getChatHistory(chatid)
{
	var data = {};
	data.action = 'gethistory';
	data.token = token;
	data.chatId=chatid;
	try { 
		this.chat.send(JSON.stringify(data));  
	} catch(ex) { 
		console.error(ex); 
	}
	return
}

function changeChat(chatid)
{
	if (chatid != 'general')
	{
		$('#chatoptions').html('<a href="#" onclick="changeChat(\'general\')">Revenir au chat général</a>');
	}
	else
	{
		$('#chatoptions').html('');
	}
	app.chatDOM.innerHTML = "<p>Changement de salon : "+chatid+"<p>";
	var data = {};
	data.action = 'setchatid';
	data.token = token;
	data.chatId=chatid;
	try { 
		this.chat.send(JSON.stringify(data));  
	} catch(ex) { 
		console.error(ex); 
	}
	getChatHistory(chatid);
	var content = document.getElementById('chatcontent');
	setTimeout(function () {content.scrollTop = content.scrollHeight + $('#chatcontent').height()},100);
	return
}

function sendChat(formname)
{
	var data = {};
  for (var i = 0, ii = get(formname).length; i < ii; ++i) {
	var input = get(formname)[i];
	if (input.name) {
	  data[input.name] = input.value;
	}
  }
  try { 
		this.chat.send(JSON.stringify(data));  
	} catch(ex) { 
		console.error(ex); 
	}
	var content = document.getElementById('chatcontent');
	setTimeout(function () {content.scrollTop = content.scrollHeight + $('#chatcontent').height()},50);
	//$('#chatcontent').animate({ scrollTop: $('#chatcontent').prop("scrollHeight") + $('#chatcontent').height() }, 3000);
	$('#chatmsginput').val('');
	return
}

function send(formname) {
	var data = {};
  for (var i = 0, ii = get(formname).length; i < ii; ++i) {
	var input = get(formname)[i];
	if (input.name) {
	  data[input.name] = input.value;
	}
  }
  try { 
		this.socket.send(JSON.stringify(data));  
	} catch(ex) { 
		console.error(ex); 
	}
	return
}

function sendJSON(jsonObject)
{
	try { 
		this.socket.send(JSON.stringify(jsonObject));  
	} catch(ex) { 
		console.error(ex); 
	}
	return
}

function sendSyncJSON(jsonObject,requestid)
{
	jsonObject.syncrequestid = requestid;
	try { 
		this.socket.send(JSON.stringify(jsonObject));  
	} catch(ex) { 
		console.error(ex); 
	}
	return
}

function generatemap(json)
{
	mapobj = JSON.parse(json);
	//mapobj = json;
	var tblBody = document.createElement("tbody");
	for (var y = 1; y <= mapobj.size_y; y++) {
    // creates a table row
    var row = document.createElement("tr");
 
    for (var x = 1; x <= mapobj.size_x; x++) {
      // Create a <td> element and a text node, make the text
      // node the contents of the <td>, and put the <td> at
      // the end of the table row
      var cell = document.createElement("td");
      cell.id="c"+x+"-"+y;
      var cellText = document.createTextNode(" ");
      cell.appendChild(cellText);
      row.appendChild(cell);
    }
 
    // add the row to the end of the table body
    tblBody.appendChild(row);
	}
	$('#missionmap').append(tblBody);
	
	if (mapobj.unwalkable)
	{
		for (var i=0; i < mapobj.unwalkable.length; i++)
		{
			element = mapobj.unwalkable[i];
			document.getElementById("c"+element.x+"-"+element.y).className='map_unwalkable';
		}
	}
	
	if (mapobj.ftl_drive)
	{
		for (var i=0; i < mapobj.ftl_drive.length; i++)
		{
			element = mapobj.ftl_drive[i];
			document.getElementById("c"+element.x+"-"+element.y).className = 'map_ftldrive';
		}
	}
	
	for (var i=0; i < mapobj.survive_system.length; i++)
	{
		element = mapobj.survive_system[i];
		document.getElementById("c"+element.x+"-"+element.y).className = 'map_survsyst';
	}
	
	if (mapobj.pilot)
	{
		for (var i=0; i < mapobj.pilot.length; i++)
		{
			element = mapobj.pilot[i];
			document.getElementById("c"+element.x+"-"+element.y).className = 'map_pilot';
		}
	}
	
	if (mapobj.communication)
	{
		for (var i=0; i < mapobj.communication.length; i++)
		{
			element = mapobj.communication[i];
			document.getElementById("c"+element.x+"-"+element.y).className = 'map_communication';
		}
	}
	
	if (mapobj.fire_zone)
	{
		for (var i=0; i < mapobj.fire_zone.length; i++)
		{
			element = mapobj.fire_zone[i];
			document.getElementById("c"+element.x+"-"+element.y).className = 'map_fire';
			document.getElementById("c"+element.x+"-"+element.y).innerText = element.value;
		}
	}
	
	if (mapobj.door)
	{
		for (var i=0; i < mapobj.door.length; i++)
		{
			element = mapobj.door[i];
			document.getElementById("c"+element.x+"-"+element.y).className='map_door';
		}
	}
}

function sendJSONmap(jsonObject)
{
	try { 
		this.map.send(JSON.stringify(jsonObject));  
	} catch(ex) { 
		console.error(ex); 
	}
	return
}

function refreshProfile()
{
	var payload = new Object();
	payload.token=token;
	payload.action='getmyplayer';
	sendJSON(payload);
}

function loadStaticContent()
{
	var payload = new Object();
	payload.token=token;
	payload.action='listmissiontypes';
	sendJSON(payload);
	payload.token=token;
	payload.action='listfactions';
	sendJSON(payload);
	payload.token=token;
	payload.action='listcharacterclasses';
	sendJSON(payload);
	payload.token=token;
	payload.action='listplayers';
	sendJSON(payload);
	refreshProfile();
}

function dispatcher(data){
	if (debug)
	{
		log(JSON.stringify(data));
	}
	if (data.syncrequestid != null)
	{
		storeDispatcher(data);
	}
	else
	{
		action = data['action'];
		switch(action) {
			case 'login':
			if ('token' in data)
			{
				token = data['token'];
				loadStaticContent();
				display('general',data);
				display('menu');
				initchat(app.host,app.chatDOM);
				setInterval(function () { var req={}; req.action='regeneratetoken'; req.token = token;sendJSON(req)},300000);
				getMyCharacterIDs(token);
				$('#tokenchat').val(token);
				$('#chat').show();
				getChatHistory('general');
			}
			else
			{
				showWarning('Mauvais login ou mot de passe');
			}
			break;
			case 'regeneratetoken':
				token = data['token'];
				$('#tokenchat').value=token;
			break;
			case 'listcharacterclasses':
				d = data['characters'];
				for (i=0;i < d.length;i++)
				{
					characterclasses[d[i].id] = d[i];
				}
			break;
			case 'listcharacters':
				characters = data['characters'];
				if (currentdisplay == 'characters')
				{
					display('characters',data);
				}
			break;
			case 'listplayers':
				players = data['players'];
				if (currentdisplay == 'players')
				{
					display('players',data);
				}
			break;
			case 'listfactions':
				factions = data['factions'];
				if (currentdisplay == 'factions')
				{
					display('factions',data);
				}
			break;
			case 'listgamesessions':
				display('gamesessions',data);
			break;
			case 'listmissiontypes':
				missiontypes = data['missiontypes'];
			break;
			case 'getcharacter':
				if (currentdisplay== 'character')
				{
					display('character',data);
				}
				else
				{
					store['character']=data;
				}
			break;
			case 'getgamesession':
				if (currentdisplay== 'gamesession')
				{
					display('gamesession',data);
				}
			break;
			case 'getmyplayer':
				myprofile = data['player'];
				displayFactionSymbol();
			break;
			case 'createcharacter':
				getMyCharacterIDs(token);
				menucharacters();
			break;
		}
	}
}

function fillToken()
{
	$('input[name=token]').each(function()
		{
			this.value = token;
		});
}

function quit(){
	if (socket != null) {
		socket.close();
		socket=null;
	}
	if (chat != null) {
		log("Goodbye!");
		chat.close();
		chat=null;
	}
}

function reconnect() {
	quit();
	initsocket(app.host);
	initchat(app.host);
	initmap(app.host);
}

function showWarning(message)
{
	$('#warningmsg').append(message);
	$('#warningbanner').show();
}
