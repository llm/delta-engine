<?php
use Doctrine\Common\Collections\ArrayCollection;
require_once __DIR__.'/../lib/Map.php';
require_once __DIR__.'/../lib/Helper.php';
use Game\Map;
/**
 * @Entity @Table(name="gamesessions")
 **/
class GameSession
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text", unique=true) **/
    protected $name;
    /** @Column(type="integer") **/
    protected $maxplayers=2;
    /** @Column(type="integer") **/
    protected $difficulty=3;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $turn=0;
    /** @Column(type="integer") **/
    protected $maxturn;
    /** @Column(type="boolean") **/
    protected $locked=false;
    /** @Column(type="boolean") **/
    protected $ended=false;
    /** @Column(type="boolean") **/
    protected $gameover=false;
     /**
     * @OneToMany(targetEntity="Character", mappedBy="gamesession", indexBy="id")
     * @var Character[]
     **/
    private $characters;
    /**
     * @OneToOne(targetEntity="Mission", cascade={"remove"})
     */
    protected $mission;
    /**
     * @ManyToOne(targetEntity="Player", inversedBy="gamesessions")
     * @var Player
     **/
    private $gamemaster;
    /**
     * @OneToMany(targetEntity="Event", mappedBy="gamesession", indexBy="id", cascade={"remove"})
     * @var Event[]
     **/
    private $events;
    /**
     * @OneToOne(targetEntity="Faction")
     */
    protected $withFaction;
    
    
    public function __construct($player,$name,$maxplayers,$mission,$maxturn,$faction=null)
    {
		$this->gamemaster = $player;
		$this->mission = $mission;
		$this->maxplayers = $maxplayers;
		$this->name=$name;
		$this->characters=new ArrayCollection();
		$this->events=new ArrayCollection();
		$this->maxturn = $maxturn;
		if (!is_null($faction))
		{
			$this->withFaction = $faction;
		}
	}
    public function getId()
    {
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}
	
	public function getCharacters()
	{
		return $this->characters->toArray();
	}
	
	public function getCharacter($id)
	{
		if (!isset($this->characters[$id]))
		{
			return false;
		}
		else
		{
			return $this->characters[$id];
		}
	}
	
	public function getEvents()
	{
		return $this->events;
	}
	
	public function addEvent($event)
	{
		$this->events[$event->getId()] = $event;
	}
	
	public function getWithFaction()
	{
		return $this->withFaction;
	}
	
	public function addCharacter($character)
	{
		if (!$this->isLocked() && count($this->characters) < $this->maxplayers)
		{
			$map = json_decode($this->getMission()->getMap(),true);
			$mapdimensions = $map['dimensions'];
			$isplaced = false;
			$cpt=0;
			while (!$isplaced)
			{
				$coord = $map['start_zone'][$cpt];
				$coordoccupied=false;
				$characters = $this->getCharacters();
				foreach ($characters as $char)
				{
					if (!$coordoccupied)
					{
						$coordoccupied = $char->getX() == $coord['x'] && $char->getY() == $coord['y'];
						if ($mapdimensions== '3' && !$mapoccupied)
						{
							$coordoccupied = $char->getZ() == $coord['z'];
						}
					}
				}
				if (!$coordoccupied)
				{
					$character->setX($coord['x']);
					$character->setY($coord['y']);
					if ($mapdimensions == '3')
					{
						$character->setZ($coord['z']);
					}
					$isplaced = true;
				}
				$cpt++;
			}
			$this->characters[$character->getId()] = $character;
			$character->setGamesession($this);
			$character->setAP($this->getAPPerTurn());
		}
		else
		{
			return false;
		}
	}
	
	public function getChatChannel()
	{
		return CHAT_CHANNEL_PREFIX.$this->getId();
	}
	
	public function removeCharacter($character)
	{
		if (!is_null($character))
		{
			$characterid = $character->getId();
			$char = $this->characters[$characterid];
			if (!is_null($char))
			{
				$this->characters->remove($characterid);
				$character->removeGameSession();
				$character->setHP($character->getMaxHP());
				return true;
			}
		}
		return false;
	}
	
	public function canRepairOnZero()
	{
		return $this->getDifficulty() < MIN_DIFFICULTY_FOR_UNREPAIRABLE_ON_ZERO;
	}
	
	public function isLocked()
	{
		return $this->locked;
	}
	
	public function isGameOver()
	{
		return $this->gameover;
	}
	
	public function setDifficulty($difficulty)
	{
		$this->difficulty = $difficulty;
	}
	
	public function getDifficulty()
	{
		return $this->difficulty;
	}
	
	public function getGameMaster()
	{
		return $this->gamemaster;
	}
	
	public function getTurn()
	{
		return $this->turn;
	}
	
	public function getMaxTurn()
	{
		return $this->maxturn;
	}
	
	public function endGame()
	{
		$this->ended = true;
	}
	
	protected function gameOver()
	{
		if (!$this->isEnded())
		{
			$this->endGame();
		}
		$this->gameover=true;
		$this->addEvent(Helper::createEvent($this,'event.gameover'));
	}
	
	public function newEvent()
	{
		$mission = $this->getMission();
		$missiontype = $mission->getType();
		$hasEvent=false;
		$event = null;
		if (rand(1,2)==1)
		{
			$hasEvent = $missiontype->hasFTLDrive() && $mission->getFTLDrive();
			if ($hasEvent)
			{
				$mission->breakFTL();
				$event = Helper::createEvent($this,'event.break.ftl');
			}
		}
		if (!$hasEvent && rand(1,2) == 1)
		{
			$hasEvent = $missiontype->hasCommunication() && $mission->getCommunications();
			if ($hasEvent)
			{
				$mission->breakCommunications();
				$event = Helper::createEvent($this,'event.break.communications');
			}
		}
		if (!$hasEvent)
		{
			$hasEvent = $mission->getSurviveSystems();
			if ($hasEvent)
			{
				$mission->breakSurviveSystems();
				$event = Helper::createEvent($this,'event.break.survivesystems');
			}
		}
		if (!$hasEvent)
		{
			$mission->takeDamage(1);
			$event = Helper::createEvent($this,'event.takedamage',array('dmg'=>1));
		}
		
		// igniting fire
		$mapjson = $mission->getMap();
		$mapobj = new Map($mapjson);
		$map = $mapobj->getMapArray();
		if (rand(1,100) <= CHANCE_OF_FIRE)
		{
			$maxx = $map['size_x'];
			$maxy = $map['size_y'];
			$maxz = 0;
			if ($map['dimensions'] == 3)
			{
				$maxz = $map['size_z'];
			}
			$found = false;
			$maxtry = 20;
			$cpt = 1;
			while (!$found && $cpt < $maxtry)
			{
				$z = 0;
				if ($map['dimensions'] == 3)
				{
					$z = rand(1,$maxz);
				}
				$x = rand(1,$maxx);
				$y = rand(1,$maxy);
				$found = !$mapobj->isFireproof($x,$y,$z);
				$cpt++;
			}
			if ($found)
			{
				$coord;
				if ($map['dimensions'] == 3)
				{
					$coord = array('x'=>$x,'y'=>$y,'z'=>$z,'value'=>1);
				}
				else
				{
					$coord = array('x'=>$x,'y'=>$y,'value'=>1);
				}
				if (array_key_exists('fire_zone',$map))
				{
					array_push($map['fire_zone'],$coord);
				}
				else
				{
					$map['fire_zone'] = array($coord);
				}
				$this->addEvent(Helper::createEvent($this,'event.new.fire'));
				$mission->setMap($map);
			}
		}
		
		if ($mission->getHP() <= 0)
		{
			$this->gameOver();
		}
		if ($this->getDifficulty() >= MIN_DIFFICULTY_FOR_GAME_OVER_ON_ZERO)
		{
			if ($mission->getFTLHP() == 0 || $mission->getSurviveSystemsHP() == 0 || $mission->getCommunicationHP() == 0)
			{
				$this->gameOver();
			}
		}
		if (!is_null($event))
		{
			$this->addEvent($event);
		}
	}
	
	public function newTurn()
	{
		$event = null;
		if ($this->getTurn()==PREPARATION_TURN)
		{
			$this->locked = true;
		}
		if ($this->getTurn() < $this->getMaxTurn())
		{
			$mission = $this->getMission();
			// Fire spreading
			$mapjson = $mission->getMap();
			$mapobj = new Map($mapjson);
			$map = $mapobj->getMapArray();
			$nbfire = 0;
			if ($mapobj->hasFire())
			{
				$firezone = &$map['fire_zone'];
				foreach ($firezone as &$node)
				{
					$value = $node['value'];
					$x = $node['x'];
					$y = $node['y'];
					$z = 0;
					$nbfire++;
					if ($map['dimensions']==3)
					{
						$z = $node['z'];
					}
					if ($value + 1 > MAX_FIRE_INTENSITY)
					{
						// spreading
						// spreading only occurs on a single level following this schematic
						// o f o
						// f F f
						// o f o
						// If you want the following schematic, you have to include the spreay in spreadx :
						// f f f
						// f F f
						// f f f
						// but this is much more difficult (maybe should be reserved for top difficulty)
						$mission->takeDamage(1);
						$spreadxs = array(-1,1);
						$spreadys = array(-1,1);
						foreach ($spreadxs as $spreadx)
						{
							$newx = $x + $spreadx;
							if ($newx >0 && $newx <= $map['size_x'] && !$mapobj->isOnFire($newx,$y,$z) && !$mapobj->isFireproof($newx,$y,$z))
							{
								$coord;
								if ($map['dimensions'] == 3)
								{
									$coord = array('x'=>$newx,'y'=>$y,'z'=>$z,'value'=>1);
								}
								else
								{
									$coord = array('x'=>$newx,'y'=>$y,'value'=>1);
								}
								array_push($map['fire_zone'],$coord);
								$nbfire++;
							}
						}
						foreach ($spreadys as $spready)
						{
							$newy = $y + $spready;
							if ($newy > 0 && $newy <= $map['size_y'] && !$mapobj->isOnFire($x,$newy,$z) && !$mapobj->isFireproof($x,$newy,$z))
							{
								$coord;
								if ($map['dimensions'] == 3)
								{
									$coord = array('x'=>$x,'y'=>$newy,'z'=>$z,'value'=>1);
								}
								else
								{
									$coord = array('x'=>$x,'y'=>$newy,'value'=>1);
								}
								array_push($map['fire_zone'],$coord);
								$nbfire++;
							}
						}
					}
					else
					{
						$node['value'] = $value+1;
					}
				}
				$mission->consumeOxygen($this->getDifficulty(),$nbfire);
				$mission->setMap($map);
			}
			
			$chance = rand(1,100);
			if ($this->getTurn() == PREPARATION_TURN || $chance < CHANCE_OF_EVENT_PER_DIFFICULTY_LEVEL * $this->getDifficulty())
			{
				$this->newEvent();
			}
			$mission->cycle($this->getDifficulty(),count($this->getCharacters()));
			if ($mission->getOxygenLevel() < OXYGEN_LEVEL_WARNING)
			{
				$damage = OXYGEN_DAMAGE_WARNING;
				if ($mission->getOxygenLevel() < OXYGEN_LEVEL_CRITICAL)
				{
					$damage = OXYGEN_DAMAGE_CRITICAL;
					$event = Helper::createEvent($this,'event.oxygen.critical');
				}
				else
				{
					$event = Helper::createEvent($this,'event.oxygen.warning');
				}
				$characters = $this->getCharacters();
				foreach ($characters as $character)
				{
					$character->setHP($character->getHP()-$damage);
					if ($character->getHP() < 0)
					{
						$event = Helper::createEvent($this,'event.character.dead',array('name'=>$character->getName()));
						$this->removeCharacter($character);
					}
				}
			}
			
			if ($mission->getHP() <= 0)
			{
				$characters = $this->getCharacters();
				foreach ($characters as $character)
				{
					$character->setHP(0);
				}
				$this->gameOver();
			}
			
			$this->turn = $this->turn + 1;
		}
		else
		{
			$this->endGame();
		}
		if (!is_null($event))
		{
			$this->addEvent($event);
		}
		if ($this->getTurn() == $this->getMaxTurn() && $this->getDifficulty() >= MIN_DIFFICULTY_FOR_NOT_ALL_SYSTEMS_ON_ZERO_LAST_ROUND)
		{
			$mission = $this->getMission();
			$missiontype = $mission->getType();
			$allzero = $mission->getSurviveSystemsHP() == 0;
			if ($allzero && $missiontype->hasFTLDrive())
			{
				$allzero = $mission->getFTLHP() == 0;
			}
			if ($allzero && $missiontype->hasCommunication())
			{
				$allzero = $mission->getCommunicationHP() == 0;
			}
			
			if ($allzero)
			{
				$this->gameOver();
			}
		}
	}
	
	public function getMaxPlayers()
	{
		return $this->maxplayers;
	}
	
	public function isPlayable()
	{
		return !$this->locked && count($this->characters) < $this->maxplayers;
	}
	
	public function getMission()
	{
		return $this->mission;
	}
	
	public function describe()
	{
		$description = array(
			'id'=>$this->getId(),
			'name'=>$this->getName(),
			'maxplayers'=>$this->getMaxPlayers(),
			'nbplayers'=>count($this->getCharacters()),
			'playable'=>$this->isPlayable(),
			'difficulty'=>$this->getDifficulty(),
			'gamemasterid'=>$this->getGameMaster()->getId(),
			'missionid'=>$this->getMission()->getId(),
			'turn'=>$this->getTurn(),
			'maxturn'=>$this->getMaxTurn(),
			'locked'=>$this->isLocked(),
			'ended'=>$this->isEnded(),
			'chatchannel'=>$this->getChatChannel(),
		);
		$charactersarray = array();
		$characters = $this->getCharacters();
		foreach ($characters as $character)
		{
			array_push($charactersarray,$character->getId());
		}
		$description['characterids'] = $charactersarray;
		if (!is_null($this->getWithFaction()))
		{
			$description['factionid'] = $this->getWithFaction()->getId();
		}
		return $description;
	}
	
	public function getAPPerTurn()
	{
		if ($this->getDifficulty()==MAX_DIFFICULTY)
		{
			return 3;
		}
		else
		{
			return 6;
		}
	}
	
	public function isEnded()
	{
		return $this->ended;
	}
	
	public function endTurn()
	{
		if ($this->isEnded())
		{
			return false;
		}
		$characters = $this->getCharacters();
		$everyoneended = true;
		foreach ($characters as $character)
		{
			$everyoneended = $everyoneended && $character->hasEndedTurn();
		}
		if ($everyoneended)
		{
			foreach ($characters as $character)
			{
				$character->setAP($this->getAPPerTurn());
				$character->newTurn();
			}
			$this->newTurn();
		}
	}
}
