<?php

class WebSocketUser {

  public $socket;
  public $id;
  public $headers = array();
  public $handshake = false;

  public $handlingPartialPacket = false;
  public $partialBuffer = "";

  public $sendingContinuous = false;
  public $partialMessage = "";
  
  public $hasSentClose = false;
  
  public $chatId='general';

  function __construct($id, $socket) {
    $this->id = $id;
    $this->socket = $socket;
  }
  
  function setChatId($id)
  {
	  $this->chatId=$id;
  }
  
  function getChatId()
  {
	  return $this->chatId;
  }
}
