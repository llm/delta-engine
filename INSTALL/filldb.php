<?php

use Tools\Tools;

require_once __DIR__."/../bootstrap.php";
require_once __DIR__."/../lib/Tools.php";
require_once __DIR__."/../const.php";

$game = $entityManager->getRepository('Game')->find(1);

if (is_null($game))
{
	
	$game = new Game();
	$game->init();
	$entityManager->persist($game);

// Faction

$factions = array (
					array('name'=>'faction.solar.federation','maxplayers'=>-1,'xpsave'=>0.8,'taxrate'=>0.5,'xptax'=>0.01),
					array('name'=>'faction.kering.empire','maxplayers'=>-1,'xpsave'=>0.7,'taxrate'=>0.4,'xptax'=>0.01),
					array('name'=>'faction.andromeda.republic','maxplayers'=>-1,'xpsave'=>0.6,'taxrate'=>0.3,'xptax'=>0),
					array('name'=>'faction.independant.consortium','maxplayers'=>20,'xpsave'=>1,'taxrate'=>0.95,'xptax'=>0.02),
					array('name'=>'faction.pirates','maxplayers'=>5,'xpsave'=>0.2,'taxrate'=>0.01,'xptax'=>0.05),
				);
foreach ($factions as $f)
{
	$faction = new Faction($f['name'],$f['maxplayers'],$f['xpsave'],$f['taxrate'],$f['xptax']);
	$entityManager->persist($faction);
}

$entityManager->flush();

// CharacterClass

$characterclasses = array(
						array('id'=>1,'name'=>'class.pilot','repair'=>1,'speed'=>1,'medic'=>0,'pilot'=>2,'engineering'=>0),
						array('id'=>2,'name'=>'class.engineer','repair'=>2,'speed'=>1,'medic'=>0,'pilot'=>0,'engineering'=>2),
						array('id'=>3,'name'=>'class.technician','repair'=>3,'speed'=>1,'medic'=>0,'pilot'=>0,'engineering'=>1),
						array('id'=>4,'name'=>'class.commander','repair'=>1,'speed'=>1,'medic'=>0,'pilot'=>1,'engineering'=>1),
						array('id'=>5,'name'=>'class.medic','repair'=>0,'speed'=>1,'medic'=>1,'pilot'=>0,'engineering'=>0)
				);
				
foreach($characterclasses as $cclass)
{
	$class = new CharacterClass($cclass['id'],$cclass['name']);
	$class->setRepair($cclass['repair']);
	$class->setSpeed($cclass['speed']);
	$class->setMedic($cclass['medic']);
	$class->setPilot($cclass['pilot']);
	$class->setEngineering($cclass['engineering']);
	$entityManager->persist($class);
}

$entityManager->flush();

// MissionType

$missiontypes = array(
				array('name'=>'ship.explorer.name','maxplayers'=>1,'maxftlhp'=>10,'maxsshp'=>10,'maxcomhp'=>10,'hp'=>5,'goldreward'=>50),
				array('name'=>'ship.cargo.name','maxplayers'=>4,'maxftlhp'=>25,'maxsshp'=>25,'maxcomhp'=>10,'hp'=>15,'goldreward'=>500),
	);
foreach ($missiontypes as $missiontype)
{
	$type = new MissionType();
	$type->setName($missiontype['name']);
	$map = '{}';
	$file = __DIR__.'/../maps/'.$missiontype['name'].'.map';
	if (file_exists($file))
	{
		$map = file_get_contents($file);
	}
	$type->setMap($map);
	$type->setMaxHp($missiontype['hp']);
	$type->setMaxPlayers($missiontype['maxplayers']);
	$type->setMaxFTLHp($missiontype['maxftlhp']);
	$type->setMaxSurviveSystemsHp($missiontype['maxsshp']);
	$type->setMaxCommunicationHp($missiontype['maxcomhp']);
	$type->setGoldReward($missiontype['goldreward']);
	
	$entityManager->persist($type);
}

$entityManager->flush();

}
else
{
	echo "Game already set";
}
