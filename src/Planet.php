<?php
/**
 * @Entity @Table(name="planets")
 **/
 
 use Doctrine\Common\Collections\ArrayCollection;
 
class Planet
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @Column(type="integer",options={"default"=10}) **/
    protected $maxequipments = 10;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $population = 0;
    /** @ManyToOne(targetEntity="Faction", inversedBy="players")
     * @var Faction
     */
    private $faction;
    
    
    public function __construct($name,$materialproduction,$faction)
    {
        $this->equipments = new ArrayCollection();
        $this->name = $name;
        $this->materialproduction = $materialproduction;
        $this->faction = $faction;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getFaction()
    {
		return $this->faction;
	}
	
	public function setFaction($faction)
	{
		$this->faction = $faction;
	}
    
}
