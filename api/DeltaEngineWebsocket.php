<?php

//require_once __DIR__.'/../lib/session.inc.php';

use Tools\Error;

require_once __DIR__.'/../lib/Helper.php';
require_once __DIR__.'/DeltaEngineWebsocketCommander.php';
require_once __DIR__.'/../lib/Error.php';

class DeltaEngineWebsocket extends WebSocketServer 
{
    //protected $maxBufferSize = 1048576; //1MB... overkill for an echo server, but potentially plausible for other applications.
    
    /**
     * 
     * 
     */
    protected function process ($user, $message) 
    {
		$reply=null;
		$replydata=array();
		$tokenIsValid=false;
		$action=false;
		$errorCode=0;
		$success=false;
        if($message == 'help')
        {
            $reply = 'I need somebody.';
        }
        else
        {
			$json = json_decode($message,true);
			if (is_array($json))
			{
				$maybeunprivileged=false;
				$hastoken = array_key_exists('token',$json);
				$skipencode=false;
				$hasrequestid = array_key_exists('syncrequestid',$json);
				$requestid;
				if ($hasrequestid)
				{
					$requestid = $json['syncrequestid'];
				}
				
				if(array_key_exists('action',$json))
				{
					$action=$json['action'];
				}
				if($hastoken)
				{
					$tokenIsValid = Helper::checkToken($json['token']);
				}
				if ($action == 'help')
				{
					$reply = $this->getHelp($tokenIsValid);
				}
				else
				{
					if ($tokenIsValid)
					{
						// privileged actions
						$token = $json['token'];
						$player = Helper::getPlayer($token,false);
						switch ($user->requestedResource) {
							case "/":
								switch ($action) {
									case 'regeneratetoken':
										$newtoken = Helper::regenerateToken($token);
										if (!$newtoken)
										{
											$reply='msg.fail';
											
										}
										else
										{
											$reply='msg.success';
											$replydata['token']=$newtoken;
											$success=true;
										}
									break;
									case 'listcharacters':
										$reply = 'msg.success';
										$replydata['characters']=Helper::listCharacters($player);
										$success=true;
									break;
									case 'listcharacterclasses':
										$reply = 'msg.success';
										$replydata['characters']=Helper::listCharacterClasses();
										$success=true;
									break;
									case 'listmissiontypes':
										$reply = 'msg.success';
										$replydata['missiontypes']=Helper::listMissionTypes();
										$success=true;
									break;
									case 'listgamesessions':
										$reply = 'msg.success';
										$replydata['gamesessions']=Helper::listGameSessions($player,$json);
										$success=true;
									break;
									case 'listdifficulties':
										$reply = 'msg.success';
										$replydata['difficulties'] = array();
										for ($i=MIN_DIFFICULTY; $i <= MAX_DIFFICULTY; $i++)
										{
											array_push($replydata['difficulties'],$i);
										}
										$success=true;
									break;
									case 'listplayers':
										$reply = 'msg.success';
										$replydata['players']=Helper::listPlayers();
										$success=true;
									break;
									case 'listfactions':
										$reply = 'msg.success';
										$replydata['factions']=Helper::listFactions();
										$success=true;
									break;
									case 'listequipments':
										$reply = 'msg.success';
										$replydata['equipments']=Helper::listEquipments();
										$success=true;
									break;
									case 'joingamesession':
										if (!Helper::joinGameSession($player,$json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success=true;
										}
									break;
									case 'createcharacter':
										if (!Helper::createCharacter($player,$json))
										{
											$reply='msg.fail';
										}
										else
										{
											$reply='msg.success';
											$success=true;
										}
									break;
									case 'creategamesession':
										if (!Helper::createGameSession($player,$json))
										{
											$reply='msg.fail';
										}
										else
										{
											$reply='msg.success';
											$success=true;
										}
									break;
									case 'getcharacter':
										$character = Helper::getCharacter($player,$json);
										if (!$character)
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$replydata['character'] = $character;
											$success=true;
										}
									break;
									case 'getmissiontype':
										$missiontype = Helper::getMissionType($json);
										if (!is_null($missiontype))
										{
											$reply = 'msg.success';
											$replydata['missiontype'] = $missiontype;
											$success=true;
										}
										else
										{
											$reply = 'msg.fail';
										}
									break;
									case 'getgamesession':
										$gamesession = Helper::getGameSession($json);
										if (!is_null($gamesession))
										{
											$reply = 'msg.success';
											$replydata['gamesession'] = $gamesession;
											$success=true;
										}
										else
										{
											$reply = 'msg.fail';
										}
									break;
									case 'getmyplayer':
										$reply = 'msg.success';
										$replydata['player'] = $player->describe();
										$success=true;
									break;
									case 'getplayer':
										$rplayer = Helper::getPlayer($json,true);
										if (!is_null($rplayer))
										{
											$reply = 'msg.success';
											$replydata['player'] = $rplayer;
											$success=true;
										}
										else
										{
											$reply = 'msg.fail';
										}
									break;
									case 'getmission':
										$mission = Helper::getMission($json);
										if (!is_null($mission))
										{
											$reply = 'msg.success';
											$replydata['mission'] = $mission;
											$success=true;
										}
										else
										{
											$reply = 'msg.fail';
										}
									break;
									case 'getevents':
										$reply = 'msg.success';
										$replydata['events'] = Helper::getEvents($json);
										$success = true;
									break;
									case 'getfactionrelations':
										$relations = Helper::getFactionRelations($json);
										$reply = 'msg.success';
										$replydata['relations'] = $relations;
										$success = true;
									break;
									case 'endturn':
										if (!Helper::endTurn($player,$json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success=true;
										}
									break;
									case 'act':
										if (!Helper::act($player,$json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success = true;
										}
									break;
									case 'reward':
										if (!Helper::getReward($player,$json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success=true;
										}
									break;
									case 'moveto':
										if (!Helper::moveTo($json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success=true;
										}
									break;
									case 'deltamoveto':
										if (!Helper::deltaMoveTo($json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success=true;
										}
									break;
									case 'updatecharacter':
										if (!Helper::updateCharacter($player,$json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success=true;
										}
									break;
									case 'changefaction':
										if (!Helper::changeFaction($player,$json))
										{
											$reply = 'msg.fail';
										}
										else
										{
											$reply = 'msg.success';
											$success = true;
										}
									break;
									default:
										$maybeunprivileged=true;
									break;
								}
							break;
							case "/chat":
								DeltaEngineWebsocketCommander::chat($this,$action,$user,$json,$reply,$player);
								$success=true;
							break;
							case "/map":
								switch($action)
								{
									case 'getmissiontypemap':
										$missiontype = Helper::getMissionType($json,true);
										if (!is_null($missiontype))
										{
											$reply = 'msg.success';
											$replydata = $missiontype->getMap();
											$skipencode=true;
											$success=true;
										}
										else
										{
											$reply = 'msg.fail';
										}
									break;
									case 'getmissionmap':
										$mission = Helper::getMission($json,true);
										if (!is_null($mission))
										{
											$reply = 'msg.success';
											$replydata = $mission->getMap();
											$skipencode=true;
											$success=true;
										}
										else
										{
											$reply = 'msg.fail';
										}
									break;
								}
							break;
						}
					}
					
					if (!$hastoken || $maybeunprivileged)
					{
						// unprivileged actions
						switch ($user->requestedResource) {
							case "/":
								switch ($action) {
									case 'login':
										$token = Helper::login($json['login'],$json['password']);
										if (!$token)
										{
											$reply = 'msg.login.fail';
											$errorCode = Error::BAD_CREDENTIALS;
										}
										else
										{
											$reply = 'msg.login.success';
											echo "${json['login']} logged in.\n";
											$replydata['token']=$token;
											$success=true;
										}
									break;
									case 'register':
										if (Helper::register($json['login'],$json['password'],$json['email']))
										{
											$reply = 'msg.user.creation.success';
											$replydata['login']=$json['login'];
											$replydata['email']=$json['email'];
											$success=true;
										}
										else
										{
											$reply = 'msg.user.creation.fail';
										}
									break;
									default:
										$reply = 'msg.no.such.action';
									break;
								}
							break;
						}
					}
					
					if ($hastoken && !$tokenIsValid)
					{
						$reply = 'msg.invalid.token';
						$errorCode = Error::INVALID_TOKEN;
						$success=true;
					}
				}
			}
		}
        if (!is_null($reply))
        { 
			$msg;
			if (!$skipencode)
			{
				$replyarray=array();
				
				if ($errorCode != 0)
				{
					$replyarray['errorcode'] = $errorCode;
				}
				
				if ($hasrequestid)
				{
					$replyarray['syncrequestid']=$requestid;
				}
				
				$replyarray['action']=str_replace('/','',$action);
				$replyarray['success']=$success;
				$replyarray['msg']=$reply;
				foreach ($replydata as $key=>$value)
				{
					$replyarray[$key]=$value;
				}
				$msg = json_encode($replyarray);
			}
			else
			{
				$msg = $replydata;
			}
			$this->send($user, $msg);
		}
         
    }
     
    /**
        This is run when socket connection is established. Send a greeting message
    */
    protected function connected ($user) 
    {
        //Send welcome message to user
        $welcome_message = 'Welcome to Delta Engine Websocket server.';
        $this->send($user, $welcome_message);
    }
     
    /**
        This is where cleanup would go, in case the user had any sort of
        open files or other objects associated with them.  This runs after the socket 
        has been closed, so there is no need to clean up the socket itself here.
    */
    protected function closed ($user) 
    {
        echo "User closed connection";
    }
    
    public function sendToAllChatUsers($chatId,$msg)
    {
		foreach ($this->users as $connectedUser)
		{
			if ($connectedUser->getChatId() == $chatId)
			{
				$this->send($connectedUser,$msg);
			}
		}
	}
	
	protected function getHelp($privileged)
	{
		if ($privileged)
		{
			return 'privileged help';
		}
		else
		{
			return 'unprivileged help';
		}
	}
}
